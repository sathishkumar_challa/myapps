package com.sparsh.miapps.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Window;

//import com.crashlytics.android.Crashlytics;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.sparsh.miapps.R;
import com.sparsh.miapps.application.MyApplication;
import com.sparsh.miapps.utils.Utils;

//import io.fabric.sdk.android.Fabric;

public class BaseActivity extends AppCompatActivity {
	FirebaseAnalytics mFirebaseAnalytics;
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);

		try
		{
		 mFirebaseAnalytics =  FirebaseAnalytics.getInstance(this);
		}catch (Exception e)
		{

		}

/*
		Bundle bundle = new Bundle();
		bundle.putString(FirebaseAnalytics.Param.ITEM_ID, id);
		bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, name);
		bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
		mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)
*/
/*
		final Fabric fabric = new Fabric.Builder(this)
				.kits(new Crashlytics())
				.debuggable(true)  // Enables Crashlytics debugger
				.build();
		Fabric.with(fabric);
        FirebaseApp.initializeApp(this);*/


		requestWindowFeature(Window.FEATURE_NO_TITLE);
		MyApplication.setCurrentActivityContext(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		MyApplication.setCurrentActivityContext(this);


	}


	@Override
	protected void onRestart() {
		super.onRestart();
		MyApplication.setCurrentActivityContext(this);
	}



	public void showAlertDialog(String title, int msgId, final boolean isFinish) {
		showAlertDialog(title, Utils.getStringFromResources(msgId,this),isFinish, null);
	}

	public void showAlertDialog(String title, String msg,final boolean isFinish) {
		showAlertDialog(title, msg,isFinish, null);
	}

	protected void showAlertDialog(String title, String msg,final boolean isFinish, final Intent intent) {

		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

		// Setting Dialog Title
		alertDialog.setTitle(TextUtils.isEmpty(title)?"":title);

		// Setting Dialog Message
		alertDialog.setMessage(msg);

		// setting Dialog cancelable to false 9010864578
		alertDialog.setCancelable(false);

		// On pressing Settings button
		alertDialog.setPositiveButton(Utils.getStringFromResources(R.string.ok_label,this),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();


						if(intent!=null)
						{
							startActivity(intent);
						}
						if(isFinish)
						{
							finish();
						}
					}
				});


		alertDialog.show();

	}

	protected void rateIt() {
		startActivity(new Intent(Intent.ACTION_VIEW,
				Uri.parse("https://play.google.com/store/apps/details?id=com.sathishinfo.myapps")));
	}


}
