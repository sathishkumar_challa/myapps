package com.sparsh.miapps.activities;

import android.app.AlertDialog;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;


import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.sparsh.miapps.R;
import com.sparsh.miapps.application.MyApplication;
import com.sparsh.miapps.fragments.AbstractFragment;
import com.sparsh.miapps.fragments.HomeFragment;
import com.sparsh.miapps.interfaces.HomeActivityListener;
import com.sparsh.miapps.utils.Constants;
import com.sparsh.miapps.utils.SharedPreferenceUtils;
import com.sparsh.miapps.utils.Utils;


/**
 * Created by sathishkumar on 27/3/17.
 */

public class MyAppsActivity extends AbstractMenuActivity implements HomeActivityListener {

    //private AppListAdapter adapter;

    private MaterialSearchView searchView;

    private AlertDialog titlealertDialog;

    //private EditText editText;
    // private AdView mAdView1;

    private Toolbar toolbar;
    private FrameLayout bottom_frame;
    private TextView count_tv, title_logo_tv;
    private ImageView cross_mark_iv, search_imageView;



    /* @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.my_apps_layout);
        init();
    }*/

    @Override
    protected void setMyContectntView() {


        super.setMyContectntView();
        myinit();
        setSearchBar();
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);

        return true;
    }*/

    private void setSearchBar() {
        searchView = (MaterialSearchView) findViewById(R.id.search_view_new);


        searchView.setOnQueryTextListener(getSearchViewListener());


        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //isSearchOpen = true;
                //Do some magic
                // if (productsAdapter!= null)
                // productsAdapter.refreshWithData(new ArrayList<ProductsResponseModule.Product>(), true);

            }

            @Override
            public void onSearchViewClosed() {
                //isSearchOpen = false;
                //Do some magic
                searchView.dismissSuggestions();
                // productsAdapter = null;
                //loadProducts();
            }
        });
    }

    @NonNull
    private MaterialSearchView.OnQueryTextListener getSearchViewListener() {
        return new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Do some magic
                //showToastMessage(query);

                /*if(MyApplication.getSearchDeals()==null || MyApplication.getSearchDeals().size()==0)
                {
                    showToastMessage("Empty search result\nPlease try again");
                    return false;
                }*/
                // searchView.closeSearch();
/*
                Intent intent= new Intent(HomeActivity.this,CategoryAllDealsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra(Constants.Pref.IS_FROM_SEARCH_RESULT_KEY,true);
                startActivity(intent);*/

                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                // callSearchDealsMapper(newText);

                if (homeFragment != null) {
                    MyApplication.showTransparentDialog1();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            MyApplication.hideTransaprentDialog1();
                        }
                    }, 3000);
                    homeFragment.searchAppByString(s.toString(), new Filter.FilterListener() {
                        /**
                         * <p>Notifies the end of a filtering operation.</p>
                         *
                         * @param count the number of values computed by the filter
                         */
                        @Override
                        public void onFilterComplete(int count) {
                            MyApplication.hideTransaprentDialog1();
                                                          /* new Handler().postDelayed(new Runnable() {
                                                               @Override
                                                               public void run() {
                                                                   MyApplication.hideTransaprentDialog();
                                                               }
                                                           },1);*/

                        }
                    });

                }
                if (TextUtils.isEmpty(s.toString())) {
                    cross_mark_iv.setVisibility(View.GONE);
                } else {
                    cross_mark_iv.setVisibility(View.VISIBLE);
                }

                return true;
            }
        };
    }


    private void myinit() {
        bottom_frame = findViewById(R.id.bottom_frame);
        search_imageView = findViewById(R.id.search_imageView);
        search_imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (searchView != null) {
                    searchView.setOnQueryTextListener(null);
                    searchView.showSearch(true);
                    searchView.setOnQueryTextListener(getSearchViewListener());
                }

            }
        });


        // mAdView = findViewById(R.id.adView);
       /* try {
            initGoogleAdd();
        }catch (Exception ex)
        {

        }*/


        // editText = (EditText) findViewById(R.id.search_edit_text);
        //editText.setText(Utils.getDeviceId());
        //Utils.showToastMessage(Utils.getDeviceId()+"");
        cross_mark_iv = (ImageView) findViewById(R.id.cross_mark_iv);
        // hideKeyBord(editText);
        cross_mark_iv.setVisibility(View.GONE);


        /*editText.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                            }

                                            @Override
                                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                //myadapter.getFilter().filter(s.toString());
                                            }

                                            @Override
                                            public void afterTextChanged(Editable s) {
                                                if (homeFragment != null) {
                                                    MyApplication.showTransparentDialog();
                                                    homeFragment.searchAppByString(s.toString(), new Filter.FilterListener() {
                                                        *//**
         * <p>Notifies the end of a filtering operation.</p>
         *
         * @param count the number of values computed by the filter
         *//*
                                                        @Override
                                                        public void onFilterComplete(int count) {
                                                            MyApplication.hideTransaprentDialog();
                                                          *//* new Handler().postDelayed(new Runnable() {
                                                               @Override
                                                               public void run() {
                                                                   MyApplication.hideTransaprentDialog();
                                                               }
                                                           },1);*//*

                                                        }
                                                    });

                                                }
                                                if (TextUtils.isEmpty(s.toString())) {
                                                    cross_mark_iv.setVisibility(View.GONE);
                                                } else {
                                                    cross_mark_iv.setVisibility(View.VISIBLE);
                                                }
                                            }
                                        }

        );*/

        /*cross_mark_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText != null) {
                    editText.setText("");
                }
            }
        });*/

    }

    @Override
    public void onResume() {
        super.onResume();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hideKeyBord(searchView);
            }
        }, 300);
        hideKeyBord(searchView);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                adHolder(bottom_frame);
            }
        }, 2000);


    }


    @Override
    protected void setActionBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //setSupportActionBar(toolbar);
        // setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        //  getSupportActionBar().setTitle("");
        //setTitleBarName("My Apps");
        //getSupportActionBar().setDisplayShowHomeEnabled(true);
        // getSupportActionBar().setHomeButtonEnabled(true);
        count_tv = findViewById(R.id.count_tv);

        count_tv.setVisibility(View.GONE);

        title_logo_tv = findViewById(R.id.title_logo_tv);

        final String title = SharedPreferenceUtils.getStringValueFromSharedPrefarence(Constants.Pref.APP_TITLE_KEY, Utils.getStringFromResources(R.string.app_name));
        title_logo_tv.setText(title);
/*
        title_logo_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showTitleAletrDialog();
            }
        });*/

        //title_logo_iv.setVisibility(View.VISIBLE);


       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            getSupportActionBar().setHomeAsUpIndicator(
                    getResources().getDrawable(R.drawable.menu));
        }*/
        super.setActionBar(true);
    }

    public void closeTitleDialog() {
        if (titlealertDialog != null && titlealertDialog.isShowing()) {
            titlealertDialog.dismiss();
        }
    }

    protected void showTitleAletrDialog() {
        if (title_logo_tv == null) {
            return;
        }

        if (titlealertDialog != null && titlealertDialog.isShowing()) {
            titlealertDialog.dismiss();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        final View view = getLayoutInflater().inflate(R.layout.title_name_layout, null);
        hideKeyBord(view);
        final EditText title_name_et = view.findViewById(R.id.title_name_et);

        final String oldTitle = SharedPreferenceUtils.getStringValueFromSharedPrefarence(Constants.Pref.APP_TITLE_KEY, Utils.getStringFromResources(R.string.app_name));

        title_name_et.setText(oldTitle);
        Button cancel_btn = view.findViewById(R.id.cancel_btn);
        Button set_btn = view.findViewById(R.id.set_btn);

        cancel_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (titlealertDialog != null && titlealertDialog.isShowing()) {
                    titlealertDialog.dismiss();
                }
                hideKeyBord(view);
            }
        });

        set_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newtitle = title_name_et.getText().toString();
                if (TextUtils.isEmpty(newtitle)) {
                    Utils.showToastMessage("Please enter valid title name");
                    return;
                }
                SharedPreferenceUtils.setStringValueToSharedPrefarence(Constants.Pref.APP_TITLE_KEY, newtitle);
                title_logo_tv.setText(newtitle);
                if (titlealertDialog != null) {
                    titlealertDialog.dismiss();
                }
                hideKeyBord(view);
            }
        });


        builder.setView(view);
        titlealertDialog = builder.create();
        titlealertDialog.show();
        closeDialogViews();
    }

    @Override
    protected AbstractFragment getFragment() {
        homeFragment = new HomeFragment();
        homeFragment.setOnRecyclerViewObjListener(new HomeFragment.RecyclerViewObjListener() {
            @Override
            public void getRecyclerView(RecyclerView recyclerView) {
                recyclerView.addOnScrollListener(new HidingScrollListener() {
                    @Override
                    public void onHide() {
                        hideViews();
                    }

                    @Override
                    public void onShow() {
                        showViews();
                    }
                });
            }

            @Override
            public void refreshEditext() {
                /*if (editText != null) {
                    editText.setText(null);
                }*/
                if (searchView != null) {
                    //editText.setText(null);
                    searchView.closeSearch();
                }
            }

            @Override
            public void applicationsCount(int count) {
                if (count_tv != null) {
                    count_tv.setText(count + " apps");
                }
            }
        });

        return homeFragment;
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        /*if(editText!=null) {
            editText.setText("");
        }*/

       /* try {
            initGoogleAdd();
        }catch (Exception ex)
        {

        }*/
    }

    private void hideViews() {
        toolbar.animate().translationY(-toolbar.getHeight()).setInterpolator(new AccelerateInterpolator(2));

        if (bottom_frame != null) {
            bottom_frame.setVisibility(View.GONE);
            //bottom_frame.animate().translationY(bottom_frame.getHeight()).setInterpolator(new AccelerateInterpolator(2));
        }

        if (toolbar != null) {
            toolbar.setVisibility(View.GONE);
        }
  /*
  FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mFabButton.getLayoutParams();
        int fabBottomMargin = lp.bottomMargin;
        mFabButton.animate().translationY(mFabButton.getHeight()+fabBottomMargin).setInterpolator(new AccelerateInterpolator(2)).start();
*/
    }

    private void showViews() {
        toolbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (bottom_frame != null) {
                    bottom_frame.setVisibility(View.VISIBLE);
                    if (bottom_frame.getChildCount() == 0) {
                        adHolder(bottom_frame);
                    }
                    //bottom_frame.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
                }
            }
        }, 1500);
        if (toolbar != null) {
            toolbar.setVisibility(View.VISIBLE);
        }
        //mFabButton.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
    }


    public void onShareClick(View view) {

    }


    public void onRefreshClick(View view) {
        if (homeFragment != null) {
            homeFragment.onRefreshClick(view);
        }
    }


    @Override
    public void showToolBar() {
        showViews();
    }

    public void adHolder(final FrameLayout frameLayout) {
        if (frameLayout == null) {
            return;
        }
        frameLayout.removeAllViews();
    }

}
