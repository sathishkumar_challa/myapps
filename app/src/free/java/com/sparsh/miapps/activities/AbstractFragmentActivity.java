package com.sparsh.miapps.activities;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.sparsh.miapps.R;
import com.sparsh.miapps.utils.Constants;
import com.sparsh.miapps.utils.SharedPreferenceUtils;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.core.app.NotificationCompat;

public abstract class AbstractFragmentActivity extends AbstractMenuActivity {


    private FirebaseRemoteConfig mFirebaseRemoteConfig;

    private String custom_dialog_to_user="custom_dialog_to_user";
    private String display_notification="display_notification";
    private String CURRENT_VERTION_NAME = "current_vertion_name";

    public static final  String is_display_banner_add ="is_display_banner_add";
    public static final   String is_display_reward_add = "is_display_reward_add";
    public static final   String is_display_intestrial_add = "is_display_intestrial_add";

    public static final String if_true_checks_count_key="if_true_checks_count";

    public static final   String is_show_config_dialog = "is_show_config_dialog";
    public static final   String intestrial_ad_delay_clicks = "intestrial_ad_delay_clicks";

    public static final   String actual_clicks_key = "actual_clicks_key";


    private final String LATEST_UPDATE_KEY="version";





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(this);

        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(true)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        //firenbaseInit();
    }

    @Override
    protected void onResume() {
        super.onResume();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                firenbaseInit();
                // Run mFirebaseRemoteConfig.fetch(timeout) here, and it works
            }
        }, 1000);
    }

    // cacheExpirationSeconds is set to cacheExpiration here, indicating the next fetch request
// will use fetch data from the Remote Config service, rather than cached parameter values,
// if cached parameter values are more than cacheExpiration seconds old.
// See Best Practices in the README for more information.
    private void firenbaseInit() {
        mFirebaseRemoteConfig.fetch(0)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete( Task<Void> task) {
                        boolean isFetch=false;
                        if (task.isSuccessful()) {
                            /*if (Constants.IS_TESTING) {
                                showToastMessage("Fetch Succeeded");
                            }*/
                            isFetch=true;


                            // After config data is successfully fetched, it must be activated before newly fetched
                            // values are returned.
                            mFirebaseRemoteConfig.activateFetched();
                        } else {

                            /*if (Constants.IS_TESTING) {
                                showToastMessage("Fetch Failed");
                            }*/
                            isFetch=false;
                        }
                        displayWelcomeMessage(isFetch);
                    }
                });
    }

    private void displayWelcomeMessage(boolean isFetch) {
        boolean is_display_banner = mFirebaseRemoteConfig.getBoolean(is_display_banner_add);
        boolean is_display_intestrial = mFirebaseRemoteConfig.getBoolean(is_display_intestrial_add);
        boolean is_display_reward = mFirebaseRemoteConfig.getBoolean(is_display_reward_add);

        boolean show_config_dialog = mFirebaseRemoteConfig.getBoolean(is_show_config_dialog);


        long count = mFirebaseRemoteConfig.getLong(if_true_checks_count_key);
        long intestrial_delay_count = mFirebaseRemoteConfig.getLong(intestrial_ad_delay_clicks);

        if(intestrial_delay_count<=0)
        {
            intestrial_delay_count=1;
        }



       // SharedPreferenceUtils.setBooleanValueToSharedPrefarence(is_display_intestrial_add,true);
        SharedPreferenceUtils.setBooleanValueToSharedPrefarence(is_display_intestrial_add,is_display_intestrial);


        SharedPreferenceUtils.setBooleanValueToSharedPrefarence(is_display_banner_add,is_display_banner);

        SharedPreferenceUtils.setBooleanValueToSharedPrefarence(is_display_reward_add,is_display_reward);
        SharedPreferenceUtils.setIntValueToSharedPrefarence(if_true_checks_count_key, (int)(count));



        // showToastMessage("isDisplayAdds :" + isDisplayAdds);

        String notification = mFirebaseRemoteConfig.getString(display_notification);
        String currentVersion = mFirebaseRemoteConfig.getString(CURRENT_VERTION_NAME);
        String version = getVersionName();

        String custom_dialog = mFirebaseRemoteConfig.getString(custom_dialog_to_user);

        if(Constants.IS_TESTING  && show_config_dialog) {

            String message =
                    "isFetch :" + isFetch + "\n" +
                    "is_display_banner :" + is_display_banner + "\n" +
                    "is_display_intestrial :" + is_display_intestrial + "\n" +
                    "is_display_reward :" + is_display_reward + "\n" +
                    "if true count :" + count + "\n\n" +
                    "notification :" + notification + "\n\n" +
                    "custom_dialog :" + custom_dialog + "\n" +
                    "firebase_version :" + currentVersion + "\n" +
                    "current_version :" + version + "\n"+
            "intestrial_delay_count :" + intestrial_delay_count + "\n";
                    ;


            showMyDialog(message);
        }



        SharedPreferenceUtils.setIntValueToSharedPrefarence(intestrial_ad_delay_clicks, (int) intestrial_delay_count);



        if (!TextUtils.isEmpty(version) && !TextUtils.isEmpty(currentVersion) && (!version.equalsIgnoreCase(currentVersion))) {
            //if (!version.equalsIgnoreCase(currentVersion)) {
                showUpdateDialog(currentVersion);
           //// }
        }else
        {

            showUserDialog(custom_dialog);

        }

        showNotificationMessage(notification);


        //showToastMessage("notification :"+notification);


    }

    private AlertDialog myDialog;
    public void showMyDialog(String message){


        if(TextUtils.isEmpty(message)) {
            return;
        }



        if (myDialog != null && myDialog.isShowing()) {
            myDialog.dismiss();
        }

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setTitle("Alert");
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });


        myDialog= alertDialogBuilder.show();


    }



    private AlertDialog notificationDialog;
    private void showNotificationMessage(String notificationJson) {

        /*
        {
  "notification_id_key": "notification_1",
  "notification_value_title": "Alert",
  "notification_value": "Hai, this is test message"
}
         */

        if(TextUtils.isEmpty(notificationJson)) {
            return;
        }

        String noti_id_key=null;
        String noti_title=null;
        String noti_message=null;

        try {
            JSONObject objectjsonObj= new JSONObject(notificationJson);
            noti_id_key=objectjsonObj.getString("notification_id_key");
            if(SharedPreferenceUtils.getBooleanValueFromSharedPrefarence(noti_id_key,false)) {
                return;
            }
            noti_title=objectjsonObj.getString("notification_value_title");
            noti_message=objectjsonObj.getString("notification_value");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(TextUtils.isEmpty(noti_id_key) || TextUtils.isEmpty(noti_message)) {
            return;
        }

        SharedPreferenceUtils.
                setBooleanValueToSharedPrefarence(
                        noti_id_key, true);
        sendNotification(this, null, noti_message, noti_title);

    }

    private AlertDialog alertDialog;
    public void showUpdateDialog(String latestVersion){
        if (SharedPreferenceUtils.
                getBooleanValueFromSharedPrefarence(
                        LATEST_UPDATE_KEY+latestVersion,
                        false)) {
            return;
        }
        if (alertDialog != null && alertDialog.isShowing()) {
            return;
        }
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setTitle(getString(R.string.youAreNotUpdatedTitle));
        alertDialogBuilder.setMessage(getString(R.string.youAreNotUpdatedMessage) + " " + latestVersion +" "+ getString(R.string.youAreNotUpdatedMessage1));
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(R.string.update, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                dialog.cancel();
            }
        });
        alertDialogBuilder.setNegativeButton("Later", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Todo Nothing


            }
        });

        alertDialog= alertDialogBuilder.show();
        SharedPreferenceUtils.
                setBooleanValueToSharedPrefarence(
                        LATEST_UPDATE_KEY+latestVersion, true);
    }


    private AlertDialog customDialog;
    public void showUserDialog(String userMessageJson){


        if(TextUtils.isEmpty(userMessageJson)) {
            return;
        }

        String message_id_key=null;
        String title=null;
        String message=null;

        try {
            JSONObject objectjsonObj= new JSONObject(userMessageJson);
            message_id_key=objectjsonObj.getString("message_id_key");
            if(SharedPreferenceUtils.getBooleanValueFromSharedPrefarence(message_id_key,false)) {
                return;
            }
            title=objectjsonObj.getString("message_value_title");
            message=objectjsonObj.getString("message_value");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(TextUtils.isEmpty(message_id_key) || TextUtils.isEmpty(title) || TextUtils.isEmpty(message)) {
            return;
        }

        if (customDialog != null && customDialog.isShowing()) {
            customDialog.dismiss();
        }

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                     dialog.cancel();
            }
        });


        customDialog= alertDialogBuilder.show();

        SharedPreferenceUtils.
                setBooleanValueToSharedPrefarence(
                        message_id_key, true);
    }


    private String getVersionName() {
        String version = "";
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return version;
    }


    private void sendNotification(Context context, Bitmap bitmap, String message, String title) {
        if (TextUtils.isEmpty(title)) {
            title = context.getString(R.string.app_name);
        }
        int icon = R.mipmap.ic_launcher;
        //	long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        Intent intent= null;
        intent= new Intent(context,MyAppsActivity.class);


        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                (int) (System.currentTimeMillis() % 100000),
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        /*NotificationCompat.Builder builder = new NotificationCompat.Builder(
                context);*/

        //start

        CharSequence name = context.getString(R.string.app_name);
        String desc = context.getString(R.string.default_notification_channel_desc);

        final String ChannelID = context.getString(R.string.default_notification_channel_id);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int imp = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(ChannelID, name,
                    imp);
            mChannel.setDescription(desc);
            mChannel.setLightColor(Color.CYAN);
            mChannel.canShowBadge();
            mChannel.setShowBadge(true);
            notificationManager.createNotificationChannel(mChannel);
        }

        //End
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, ChannelID);

        NotificationCompat.Builder builder1 = builder
                .setContentIntent(pendingIntent)
                .setSmallIcon(icon)
                .setTicker(message)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setStyle(
                        new NotificationCompat.BigTextStyle()
                                .bigText(message))
                .setContentText(message);

        if (bitmap != null) {
            builder1.setLargeIcon(bitmap);
            // builder1.setStyle(new Notification.BigPictureStyle().bigPicture(bitmap));
            builder1.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap));
        }

        Notification notification = builder1.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
//		notification.defaults |= Notification.DEFAULT_SOUND;
//		notification.defaults |= Notification.DEFAULT_VIBRATE;
        notificationManager.notify(8435/*(int) (System.currentTimeMillis() % 100000)*/,
                notification);
    }

}
