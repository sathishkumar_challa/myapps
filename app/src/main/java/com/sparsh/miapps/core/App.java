package com.sparsh.miapps.core;

import android.graphics.drawable.Drawable;

import java.io.Serializable;

/**
 * App is a bean class for holding the fields required for showing the list of
 * installed applications in the device.
 */
@SuppressWarnings("serial")
public class App implements Serializable {

	public int id;
	public String packageName;
	public String appName;
	public Drawable drawable;
	public int count=0;
	public int isUserApp=0;

	public final boolean isHeader;

	public App(boolean isHeader) {
		this.isHeader = isHeader;
	}

	/**
	 * Returns the package name of the current app.
	 * 
	 * @return package name of the current app.
	 */
	public String getPackageName() {
		return packageName;
	}

	/**
	 * Sets the package for the current app.
	 * 
	 * @param packageName
	 *            package name of the application.
	 */
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	/**
	 * Returns the application name.
	 * 
	 * @return application name.
	 */
	public String getAppName() {
		return appName;
	}

	/**
	 * Sets the application name.
	 * 
	 * @param appName
	 *            appplication name.
	 */
	public void setAppName(String appName) {
		this.appName = appName;
	}

	@Override
	public String toString() {
		return  appName;
	}


	@Override
	public boolean equals(Object obj) {
		if(obj instanceof  App )
		{
			App app= (App) obj;
			if(app.packageName!=null && packageName!=null && app.packageName.equalsIgnoreCase(packageName))
			{
				return true;
			}
		}

		return super.equals(obj);
	}
}
