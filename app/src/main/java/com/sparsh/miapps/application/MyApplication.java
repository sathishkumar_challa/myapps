package com.sparsh.miapps.application;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.ImageView;

import com.sparsh.miapps.R;

import java.io.File;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

public class MyApplication extends MultiDexApplication {
    private static MyApplication myApplication;
    private static ProgressDialog pd;
    private static Context mContext;

    @Override
    public void onCreate() {
        MultiDex.install(this);
        super.onCreate();
        myApplication = this;
        mContext = this;

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }



    public static MyApplication getInstance() {
        return myApplication;
    }

    public static Context getCurrentActivityContext() {
        return mContext;
    }

    public static void setCurrentActivityContext(Context mContext) {
        MyApplication.mContext = mContext;

    }



    public static void showTransparentDialog1(String message, String title) {//Transparent
        showTransparentDialog1(message, title, false);

    }

    public static void showTransparentDialog1(String message, String title, boolean isShaow) {//Transparent
        try {
            if (!isShaow) {
                showTransparentDialogOnly();
                return;
            }
            // if (pd != null) {
            hideTransaprentDialog1();
            pd = new ProgressDialog((Activity) getCurrentActivityContext());
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setMessage(message);
            pd.setTitle(title);
            // pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pd.setCancelable(false);
            pd.setCanceledOnTouchOutside(false);
            pd.show();
            // } else {
            // pd.dismiss();
            // pd.show();
            // }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void showTransparentDialogOnly() {//Transparent
        try {

            // if (pd != null) {
            hideTransaprentDialog1();
            pd = new ProgressDialog((Activity) getCurrentActivityContext(), R.style.MyTheme);
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            // pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            pd.setCancelable(false);
            pd.setCanceledOnTouchOutside(false);
            pd.show();
            // } else {
            // pd.dismiss();
            // pd.show();
            // }
        } catch (Exception e) {
            //e.printStackTrace();
        }

    }

    public static void showTransparentDialog1() {
        showTransparentDialog1("Fetching data..", "Loading...");
    }

    public static void showTransparentDialog1(boolean isShow) {
        showTransparentDialog1("Fetching data..", "Loading...", isShow);
    }


    public static void showTransparentDialog1(String title) {
        showTransparentDialog1("Fetching data..", title);
    }

    public static void hideTransaprentDialog1() {

        try {
            if (pd != null && pd.isShowing()) {
                pd.dismiss();
            }
            pd = null;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



/*


    public void setBitmapToImageview(int dwawableId, ImageView imageView) {

        // Uri  uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://"+getCurrentActivityContext().getPackageName()+"/drawable/" + drawableName;
        Picasso.with(this).load(dwawableId).into(imageView);

    }
    */
/*public void setBitmapToImageview(String packageName, ImageView imageView) {

         //Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://"+getCurrentActivityContext().getPackageName()+"/drawable/" + drawableName;
        Picasso.with(this).load(dwawableId).into(imageView);

    }
*//*



    public void setBitmapToImageview(int defaultImageId, ImageView imageView,
                                     String imageUrl) {
        if (TextUtils.isEmpty(imageUrl)) {
            Picasso.with(this).load(defaultImageId).placeholder(defaultImageId)
                    .into(imageView);

            return;
        }

        Picasso.with(this).load(imageUrl).placeholder(defaultImageId)
                .into(imageView);

    }

    public void setBitmapToImageview(int defaultImageId, ImageView imageView,
                                     File file) {
        if (file == null || !file.exists()) {
            Picasso.with(this).load(defaultImageId).placeholder(defaultImageId)
                    .into(imageView);

            return;
        }

        Picasso.with(this).load(file).placeholder(defaultImageId)
                .into(imageView);

    }
    //540x260     270x130
    public void setBitmapToImageview1(int defaultImageId, ImageView imageView,
                                      File file) {
        if (file == null || !file.exists()) {
            Picasso.with(this).load(defaultImageId).placeholder(defaultImageId)
                    .into(imageView);

            return;
        }

        Picasso.with(this).load(file).resize(270,130).placeholder(defaultImageId)
                .into(imageView);

    }
    public void setBitmapToImageviewCircular(int defaultImageId, ImageView imageView,
                                             String imageUrl) {
        if (TextUtils.isEmpty(imageUrl)) {
            Picasso.with(this).load(defaultImageId).placeholder(defaultImageId)
                    .into(imageView);

            return;
        }

        Picasso.with(this).load(imageUrl).transform(new CircleTransform()).placeholder(defaultImageId).into(imageView);

    }

    public void setBitmapToImageviewCircular(int defaultImageId, ImageView imageView,
                                             File file) {
        if (file == null || !file.exists()) {
            Picasso.with(this).load(defaultImageId).placeholder(defaultImageId)
                    .into(imageView);

            return;
        }

     */
/*   Picasso.with(this).load(file).placeholder(defaultImageId)
                .into(imageView);*//*


        Picasso.with(this).load(file).transform(new CircleTransform()).placeholder(defaultImageId).into(imageView);


    }


    public void setBitmapToImageview(ImageView imageView,
                                     int imageResourceId) {
        Picasso.with(this).load(imageResourceId).into(imageView);

    }

    public void setBitmapToImageviewBG(final ImageView imageView,
                                       final int imageResourceId) {
        if (true) {
            Picasso.with(this).load(imageResourceId).into(imageView);
            return;
        }
       */
/* Picasso.with(this).load(imageResourceId).into(new Target() {

            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                imageView.setBackground(new BitmapDrawable(MyApplication.getCurrentActivityContext().getResources(), bitmap));
            }

            @Override
            public void onBitmapFailed(final Drawable errorDrawable) {
                Log.d("TAG", "FAILED");
            }

            @Override
            public void onPrepareLoad(final Drawable placeHolderDrawable) {
                Log.d("TAG", "Prepare Load");
            }
        });*//*


    }

*/



}
