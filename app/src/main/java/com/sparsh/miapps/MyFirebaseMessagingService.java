package com.sparsh.miapps;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.sparsh.miapps.activities.MyAppsActivity;

import androidx.core.app.NotificationCompat;

import org.json.JSONObject;

/**
 * Created by satish on 28/11/17.
 */

//https://github.com/firebase/quickstart-android/blob/master/messaging/app/src/main/java/com/google/firebase/quickstart/fcm/MyFirebaseMessagingService.java
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
   // public static final String FCM_MESSAGE_KEY = "fcm_message_key";



    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        Log.i(TAG, "Device Token :" + token);
    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        //http://stackoverflow.com/questions/37711082/how-to-handle-notification-when-app-in-background-in-firebase

        // Check if message contains a notification payload.
        try {
            if (remoteMessage.getData() != null) {
                Log.d(TAG, "Message Notification Body: " + remoteMessage.getData());
                JSONObject jsonObject = new JSONObject(remoteMessage.getData().get("myMessage"));
                if (jsonObject != null) {
                    String title = jsonObject.getString("myTitle");
                    if (TextUtils.isEmpty(title)) {
                        title = getResources().getString(R.string.app_name);
                    }
                    String message = jsonObject.getString("myMessage");
                    if (TextUtils.isEmpty(message)) {
                        message = getResources().getString(R.string.default_notification_msg);
                    }

                    if ( jsonObject.has("is_update_app") && jsonObject.getBoolean("is_update_app")) {
                        sendUpdateNotification(getApplicationContext(), message, title);
                    } else {
                        sendNotification(getApplicationContext(), null, message, title);
                    }
                }

            } else {
                sendNotification(getApplicationContext(), null, remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    // [END receive_message]

    /*{
        "response": {
        "notification_message": "Sathish has accept your game",
                "user_id": "49",
                "oppnent_user_id": "50",
                "game_status" : "4"
        "game_id": "12"
    }
    }*/


    // private void sendCFWNotification(Context context, String iconUrl,String message,String title, String type,String offer_id) {

    private void sendUpdateNotification(Context context, String message, String title) {
        if (TextUtils.isEmpty(title)) {
            title = context.getString(R.string.app_name);
        }
        int icon = R.mipmap.ic_launcher;
        //	long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        //Intent intent= null;
        //intent= new Intent(context,MyAppsActivity.class);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName()));
        //


        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                (int) (System.currentTimeMillis() % 100000),
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        /*NotificationCompat.Builder builder = new NotificationCompat.Builder(
                context);*/

        //start

        CharSequence name = context.getString(R.string.app_name);
        String desc = context.getString(R.string.default_notification_channel_desc);

        final String ChannelID = context.getString(R.string.default_notification_channel_id);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int imp = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(ChannelID, name,
                    imp);
            mChannel.setDescription(desc);
            mChannel.setLightColor(Color.CYAN);
            mChannel.canShowBadge();
            mChannel.setShowBadge(true);
            notificationManager.createNotificationChannel(mChannel);
        }

        //End
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, ChannelID);

        NotificationCompat.Builder builder1 = builder
                .setContentIntent(pendingIntent)
                .setSmallIcon(icon)
                .setTicker(message)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setStyle(
                        new NotificationCompat.BigTextStyle()
                                .bigText(message))
                .setContentText(message);

//        if (bitmap != null) {
//            builder1.setLargeIcon(bitmap);
//            // builder1.setStyle(new Notification.BigPictureStyle().bigPicture(bitmap));
//            builder1.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap));
//        }

        Notification notification = builder1.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
//		notification.defaults |= Notification.DEFAULT_SOUND;
//		notification.defaults |= Notification.DEFAULT_VIBRATE;
        notificationManager.notify(8436/*(int) (System.currentTimeMillis() % 100000)*/,
                notification);
    }


    private void sendNotification(Context context, Bitmap bitmap, String message, String title) {
        if (TextUtils.isEmpty(title)) {
            title = context.getString(R.string.app_name);
        }
        int icon = R.mipmap.ic_launcher;
        //	long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        Intent intent = null;
        intent = new Intent(context, MyAppsActivity.class);


        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                (int) (System.currentTimeMillis() % 100000),
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        /*NotificationCompat.Builder builder = new NotificationCompat.Builder(
                context);*/

        //start

        CharSequence name = context.getString(R.string.app_name);
        String desc = context.getString(R.string.default_notification_channel_desc);

        final String ChannelID = context.getString(R.string.default_notification_channel_id);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int imp = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(ChannelID, name,
                    imp);
            mChannel.setDescription(desc);
            mChannel.setLightColor(Color.CYAN);
            mChannel.canShowBadge();
            mChannel.setShowBadge(true);
            notificationManager.createNotificationChannel(mChannel);
        }

        //End
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, ChannelID);

        NotificationCompat.Builder builder1 = builder
                .setContentIntent(pendingIntent)
                .setSmallIcon(icon)
                .setTicker(message)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setStyle(
                        new NotificationCompat.BigTextStyle()
                                .bigText(message))
                .setContentText(message);

        if (bitmap != null) {
            builder1.setLargeIcon(bitmap);
            // builder1.setStyle(new Notification.BigPictureStyle().bigPicture(bitmap));
            builder1.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap));
        }

        Notification notification = builder1.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
//		notification.defaults |= Notification.DEFAULT_SOUND;
//		notification.defaults |= Notification.DEFAULT_VIBRATE;
        notificationManager.notify(8435/*(int) (System.currentTimeMillis() % 100000)*/,
                notification);
    }

   /* public void initChannels(Context context,NotificationManager notificationManager) {
        if (Build.VERSION.SDK_INT < 26) {
            return;
        }
        NotificationChannel channel = new NotificationChannel("default",
                "Channel name",
                NotificationManager.IMPORTANCE_DEFAULT);
        channel.setDescription("Channel description");
        notificationManager.createNotificationChannel(channel);
    }
*/

}
