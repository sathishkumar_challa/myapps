package com.sparsh.miapps.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.sparsh.miapps.R;
import com.sparsh.miapps.activities.MyAppsActivity;
import com.sparsh.miapps.application.MyApplication;
import com.sparsh.miapps.core.App;
import com.sparsh.miapps.database.ApplicationsDatabase;
import com.sparsh.miapps.fragments.HomeFragment;
import com.sparsh.miapps.imageProcess.ApplicationIconDecoder;
import com.sparsh.miapps.imageProcess.PassthroughModelLoader;
import com.sparsh.miapps.utils.Constants;
import com.sparsh.miapps.utils.SharedPreferenceUtils;
import com.sparsh.miapps.utils.Utils;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

import static com.sparsh.miapps.utils.Constants.DEFAULT_ICON_SIZE;

/*import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;*/

//import com.squareup.picasso.Picasso;


public class AppListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable, SectionIndexer {
    private static final int Header_type = 1;
    private static final int NormalType = 2;

    private static float TEXT_SIZE_FACTOR = 3.5f;

    private final LayoutInflater inflater;
    private ArrayList<App> orderList;
    //private static Map<String, Drawable> cache;
    private final ArrayList<App> originalList;

    private CustomFilter filter;

    private Context context;
    private InputMethodManager keyBoardHide;
    private HomeFragment.RecyclerViewObjListener objListener;
    private ArrayList<App> recentApps;
    private int icon_size = DEFAULT_ICON_SIZE * 10;
    private int resourceId;
    int finalIconSize = 50;
    // private int maxCount;

   /* private static final int CORE_POOL_SIZE = 5;
    private static final int MAXIMUM_POOL_SIZE = 800;

    private static final BlockingQueue<Runnable> sWorkQueue =
            new LinkedBlockingQueue<Runnable>(10);

    private static final ThreadPoolExecutor sExecutor =
            new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE,10,
                     TimeUnit.SECONDS, sWorkQueue);
*/

    public AppListAdapter(Context context, /*int maxCount,*/ ArrayList<App> orderList, HomeFragment.RecyclerViewObjListener objListener) {
        this.context = context;
        // this.maxCount=maxCount;

 /*       recentApps = new ApplicationsDatabase(context)
                .getRecentApplications(SharedPreferenceUtils.getIntValueToSharedPrefarence(Constants.Pref.SPANCOUNT, 4));
*/
        TEXT_SIZE_FACTOR = context.getResources().getDimension(R.dimen._1_5sp);
        recentApps = new ApplicationsDatabase(context)
                .getRecentApplications();

        icon_size = 10* SharedPreferenceUtils.getIntValueToSharedPrefarence(Constants.Pref.APP_ICON_SIZE, Constants.DEFAULT_ICON_SIZE);


        resourceId = context.getResources().getIdentifier("_" + icon_size + "sp", "dimen", context.getPackageName());

        finalIconSize = (int) context.getResources().getDimension(resourceId);


        this.objListener = objListener;
        this.originalList = orderList;
        this.orderList = new ArrayList<>();
        this.orderList.addAll(orderList);
        this.context = context;
        filter = new CustomFilter(this);
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        initliazeCache();
    }

    private void initliazeCache() {
        //  this.cache = new HashMap<>(200);
         /*final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
         final int cacheSize = maxMemory / 2;
        cache= new HashMap<>();*/
       /* cache = new LruCache<String, Drawable>(cacheSize) {
            @SuppressLint("NewApi")
            @Override
            protected int sizeOf(String key, Drawable bitmap) {
                    return 400;
            }
        };*/

        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //notifyItemChanged(0);
                notifyDataSetChanged();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //notifyItemChanged(0);
                        notifyDataSetChanged();
                    }
                }, 1500);
            }
        }, 1500);*/
        if (objListener != null) {
            objListener.applicationsCount(orderList.size());
        }


    }

    @Override
    public int getItemViewType(int position) {
        boolean isEnable = SharedPreferenceUtils.getBooleanValueFromSharedPrefarence(Constants.Pref.IS_FREQ_USED_APPS_ENABLE, true);


        if (!isEnable) {
            return NormalType;
        }
        return orderList.get(position).isHeader ? Header_type : NormalType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == Header_type) {
            final View convertView = inflater
                    .inflate(R.layout.apps_header_layout, parent, false);
            return new HeaderHolder(convertView);
        } else {
            final View convertView = inflater
                    .inflate(R.layout.app_layout, parent, false);
            return new AppListHolder(convertView, icon_size);
        }

    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        int safePosition = holder.getAdapterPosition();
        final App app = orderList.get(safePosition);
        if (holder instanceof HeaderHolder) {

            boolean isEnable = SharedPreferenceUtils.getBooleanValueFromSharedPrefarence(Constants.Pref.IS_FREQ_USED_APPS_ENABLE, true);


            HeaderHolder headHolder = (HeaderHolder) holder;
            if (!isEnable) {
                headHolder.bindRecentApps(new ArrayList<App>());
                return;
            }


  /*          recentApps = new ApplicationsDatabase(context)
                    .getRecentApplications(SharedPreferenceUtils.getIntValueToSharedPrefarence(Constants.Pref.SPANCOUNT, 4));
*/
            recentApps = new ApplicationsDatabase(context)
                    .getRecentApplications();


            headHolder.bindRecentApps(recentApps);
            return;
        }
        AppListHolder holder1 = (AppListHolder) holder;
        holder1.setHolderData(app, false);
        //notifyItemChanged(0);
        //hideKeyBord(holder.itemView);


    }


    @Override
    public int getItemCount() {
        return orderList.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    public class HeaderHolder extends RecyclerView.ViewHolder {

        LinearLayout recent_linear, recent_main_linear;
        TextView all_apps_count_tv, recent_apps_count_tv, all_lbl_tv, frequently_lbl_tv;


        public HeaderHolder(View itemView) {
            super(itemView);
            recent_linear = itemView.findViewById(R.id.recent_linear);
            recent_main_linear = itemView.findViewById(R.id.recent_main_linear);

            recent_apps_count_tv = itemView.findViewById(R.id.recent_apps_count_tv);

            all_apps_count_tv = itemView.findViewById(R.id.all_apps_count_tv);
            frequently_lbl_tv = itemView.findViewById(R.id.frequently_lbl_tv);
            all_lbl_tv = itemView.findViewById(R.id.all_lbl_tv);


        }

        public void bindRecentApps(ArrayList<App> apps) {

            all_apps_count_tv.setText("(" + (orderList.size() - 1) + ")");
            recent_apps_count_tv.setText("(" + recentApps.size() + ")");

            all_apps_count_tv.setTextColor(SharedPreferenceUtils.getIntValueToSharedPrefarence(itemView.getResources().getString(R.string.sub_title_color), getColorCode(R.color.black, itemView)));
            recent_apps_count_tv.setTextColor(SharedPreferenceUtils.getIntValueToSharedPrefarence(itemView.getResources().getString(R.string.sub_title_color), getColorCode(R.color.black, itemView)));
            frequently_lbl_tv.setTextColor(SharedPreferenceUtils.getIntValueToSharedPrefarence(itemView.getResources().getString(R.string.sub_title_color), getColorCode(R.color.black, itemView)));
            all_lbl_tv.setTextColor(SharedPreferenceUtils.getIntValueToSharedPrefarence(itemView.getResources().getString(R.string.sub_title_color), getColorCode(R.color.black, itemView)));

            //appNametextView.setTextColor(SharedPreferenceUtils.getIntValueToSharedPrefarence(itemView.getResources().getString(R.string.app_name_color), getColorCode(R.color.black,itemView)));


            recent_linear.removeAllViews();
            if (apps == null || apps.size() == 0) {
                recent_main_linear.setVisibility(View.GONE);
            } else {
                recent_main_linear.setVisibility(View.VISIBLE);
            }

            for (App app : apps) {
                if (app == null) {
                    continue;
                }

                final View convertView = inflater
                        .inflate(R.layout.app_layout_recent, null);
                AppListHolder holder = new AppListHolder(convertView, icon_size);

                holder.setHolderData(app, true);
                recent_linear.addView(convertView);


            }
            //  recent_main_linear.setVisibility(View.VISIBLE);
        }


    }

    public class AppListHolder extends RecyclerView.ViewHolder {
        ImageView ivAppIcon;
        TextView appNametextView;
        MyImageLoadTask task;
        LinearLayout parent_linear;

        public AppListHolder(View itemView, int icon_size) {
            super(itemView);
            ivAppIcon =
                    (ImageView) itemView.findViewById(R.id.app_image_iv);
            //R.dimen._1sp
           /* new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                   // finalIconSize= (int) context.getResources().getDimension(resourceId);
                    ivAppIcon.getLayoutParams().height =finalIconSize;// (int) itemView.getResources().getDimension(resourceId);
                }
            },500);*/

            appNametextView = (TextView) itemView
                    .findViewById(R.id.app_name_tv);
            parent_linear = itemView.findViewById(R.id.parent_linear);
        }

        public void setHolderData(final App app, boolean isFromRecent) {

       /*     int resourceId = itemView.getResources().
                    getIdentifier("_" + icon_size + "sp", "dimen", itemView.getContext().getPackageName());
   */
            ivAppIcon.getLayoutParams().height = finalIconSize;

            // Drawable bitmapFromMemCache = null;//getBitmapFromMemCache(getKeyValue(app
            //.getPackageName()));
            //ivAppIcon.setBackground(null);
            // ivAppIcon.setImageDrawable(null);
            /*if (bitmapFromMemCache == null) {
                 task = new MyImageLoadTask(
                        ivAppIcon);
                task.execute(app.packageName);
            } else {
                ivAppIcon.setImageDrawable(bitmapFromMemCache);
            }*/
            /*if(app.isHeader)
            {
                itemView.setVisibility(View.GONE);
            }else
            {
                itemView.setVisibility(View.VISIBLE);
            }*/

            try {

                Glide.with(context)
                        .using(new PassthroughModelLoader<ApplicationInfo, ApplicationInfo>(), ApplicationInfo.class)
                        .from(ApplicationInfo.class)
                        .as(Drawable.class)
                        .decoder(new ApplicationIconDecoder(context))
                        .placeholder(R.drawable.loading)
                        .diskCacheStrategy(DiskCacheStrategy.NONE) // cannot disk cache ApplicationInfo, nor Drawables
                        //.load(context.getApplicationInfo())
                        .load(context.getPackageManager().getApplicationInfo(app.packageName, 0))
                        .into(ivAppIcon)
                ;
            } catch (Exception e) {
                e.printStackTrace();
            }

          /*  int index= originalList.indexOf(app);
            if(index==-1)
            {
                MyImageLoadTask task = new MyImageLoadTask(
                        ivAppIcon, null);
                task.execute(app.packageName);
            }else
            {
                if (originalList.get(index).drawable==null) {
                    MyImageLoadTask task = new MyImageLoadTask(
                            ivAppIcon, originalList.get(index));
                    task.execute(app.packageName);
                }else
                {
                    ivAppIcon.setImageDrawable(originalList.get(index).drawable);
                }

            }*/
            /*if (orderList.get(orderList.indexOf(app)).drawable==null) {
                // ivAppIcon.get().setImageResource(R.drawable.ic_launcher);
                // ivAppIcon.setBackground(null);
                // ivAppIcon.setImageDrawable(null);
                // Picasso.with(context).load(R.drawable.loading).into(ivAppIcon);
                MyImageLoadTask task = new MyImageLoadTask(
                        ivAppIcon, position);
                task.execute(app.packageName);
                //  task.executeOnExecutor(sExecutor,app.packageName);
            } else {

                //ivAppIcon.setImageBitmap(null);
                // ivAppIcon.setImageBitmap(bitmapFromMemCache);
                ////ivAppIcon.setImageDrawable(bitmapFromMemCache);
            }*/


            parent_linear.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    //Utils.showToastMsg("Now long click on " + app.getAppName());
                    showDialogForUninstall(app);
                    return true;
                }
            });

            parent_linear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!Utils.launchApplication(context, app)) {
                        Utils.showToastMsg("Application not available.. ");
                        Context context = MyApplication.getCurrentActivityContext();

                        if (context instanceof MyAppsActivity) {
                            MyAppsActivity activity = (MyAppsActivity) context;
                            activity.onRefreshClick(null);
                        }

                    }
                }
            });

          /*  ivAppIcon.get().setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Utils.showToastMsg("Now long click on "+app.getPackageName());
                    return true;
                }
            });*/

            appNametextView.setText(app.getAppName());
            appNametextView.setTextColor(SharedPreferenceUtils.getIntValueToSharedPrefarence(itemView.getResources().getString(R.string.app_name_color), getColorCode(R.color.black, itemView)));

            if (isFromRecent) {
                return;
            }
           /* int count = SharedPreferenceUtils.getIntValueToSharedPrefarence(Constants.Pref.SPANCOUNT, 4);
           int spanCount = maxCount - count;
            if (spanCount <= 0) {
                spanCount = 3;
            }
            appNametextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, TEXT_SIZE_FACTOR * spanCount);
*/
            //appNametextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, SharedPreferenceUtils.getIntValueToSharedPrefarence(Constants.Pref.SPANCOUNT,4)*3);
        }
    }

    private int getColorCode(int id, View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return view.getResources().getColor(id, null);
        } else {
            return view.getResources().getColor(id);
        }
    }


    public static ApplicationInfo getApplicationInfo(Context context, String path) {
        PackageManager pm = context.getPackageManager();
        PackageInfo info = pm.getPackageArchiveInfo(path, PackageManager.GET_ACTIVITIES);
        ApplicationInfo appInfo = null;
        if (info != null) {
            appInfo = info.applicationInfo;
            appInfo.sourceDir = path;
            appInfo.publicSourceDir = path;
        }
        return appInfo;
    }

    private ApplicationInfo getMyAppsApplicationInfo(App app) {
        PackageManager packageManager = MyApplication.getCurrentActivityContext().getPackageManager();
        try {
            return packageManager.getApplicationInfo(app.packageName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    private void shareApplication(App myApp) {

        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        ApplicationInfo app = getMyAppsApplicationInfo(myApp);

        if (app == null) {
            Utils.showToastMessage("Application not found");
            return;
        }

        //app1.getgetApplicationContext().getApplicationInfo();
        String filePath = app.sourceDir;

        Intent intent = new Intent(Intent.ACTION_SEND);


        // MIME of .apk is "application/vnd.android.package-archive".
        // but Bluetooth does not accept this. Let's use "*/*" instead.
        // intent.setType("*/*");
        intent.setType("application/vnd.android.package-archive");

        // Append file and send Intent
        File originalApk = new File(filePath);

        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(originalApk));
        MyApplication.getCurrentActivityContext().startActivity(Intent.createChooser(intent, "Share " + myApp.getAppName() + " via"));

        if (true) {
            return;
        }
/*
        try {
            //Make new directory in new location
            File tempFile = new File(MyApplication.getCurrentActivityContext().getExternalCacheDir() + "/ExtractedApk");
            //If directory doesn't exists create new
            if (!tempFile.isDirectory())
                if (!tempFile.mkdirs())
                    return;
            //Get application's name and convert to lowercase
            tempFile = new File(tempFile.getPath() + "/" + myApp.appName.replace(" ", "").toLowerCase() + ".apk");
            //If file doesn't exists create new
            if (!tempFile.exists()) {
                try {
                    if (!tempFile.createNewFile()) {
                        return;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            //Copy file to new location
            InputStream in = new FileInputStream(originalApk);
            OutputStream out = new FileOutputStream(tempFile);

            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
            System.out.println("File copied.");
            //Open share dialog
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(tempFile));
            MyApplication.getCurrentActivityContext().startActivity(Intent.createChooser(intent, "Share app via"));

        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    private AlertDialog dialog;

    private void showDialogForUninstall(final App app) {

        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        AlertDialog.Builder alertDialog = new AlertDialog.Builder((MyAppsActivity) MyApplication.getCurrentActivityContext());

        // Setting Dialog Title
        //alertDialog.setTitle("Alert");

        View view = inflater.inflate(R.layout.app_info_layout, null);
        alertDialog.setView(view);

        ImageView app_image_iv = view.findViewById(R.id.app_image_iv);

        TextView app_name_tv = view.findViewById(R.id.app_name_tv);

        try {

            Glide.with(context)
                    .using(new PassthroughModelLoader<ApplicationInfo, ApplicationInfo>(), ApplicationInfo.class)
                    .from(ApplicationInfo.class)
                    .as(Drawable.class)
                    .decoder(new ApplicationIconDecoder(context))
                    .placeholder(R.drawable.loading)
                    .diskCacheStrategy(DiskCacheStrategy.NONE) // cannot disk cache ApplicationInfo, nor Drawables
                    //.load(context.getApplicationInfo())
                    .load(context.getPackageManager().getApplicationInfo(app.packageName, 0))
                    .into(app_image_iv)
            ;
        } catch (Exception e) {
            e.printStackTrace();
        }

        app_name_tv.setText(app.getAppName());
        // app_name_tv.setTextColor(SharedPreferenceUtils.getIntValueToSharedPrefarence(view.getResources().getString(R.string.app_name_color), getColorCode(R.color.black, view)));


        ImageView playstore_iv = view.findViewById(R.id.playstore_iv);
        ImageView app_info_iv = view.findViewById(R.id.app_info_iv);
        ImageView app_delete_iv = view.findViewById(R.id.app_delete_iv);

        ImageView share_app_iv = view.findViewById(R.id.share_app_iv);

        RelativeLayout share_relative = view.findViewById(R.id.share_relative);

        RelativeLayout uninstall_relative = view.findViewById(R.id.uninstall_relative);

        if (app.isUserApp == 0) {
            share_relative.setVisibility(View.GONE);
            uninstall_relative.setVisibility(View.GONE);
        } else {
            share_relative.setVisibility(View.VISIBLE);
            uninstall_relative.setVisibility(View.VISIBLE);
        }

        playstore_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPlayStore(app);
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        app_info_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openApplicationSettings(app);
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        app_delete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unInstallApplication(app);
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        share_app_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //unInstallApplication(app);
                shareApplication(app);
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }

            }
        });


        // Setting Dialog Message
        // alertDialog.setMessage("You selected '" + app.appName + "' application for");

        // setting Dialog cancelable to false 9010864578
        //alertDialog.setCancelable(false);

        // On pressing Settings button
        /*alertDialog.setPositiveButton("Uninstall",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        unInstallApplication(app);
                    }
                });
        alertDialog.setNegativeButton("App Info", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                openApplicationSettings(app);
            }
        });
        alertDialog.setNeutralButton("Play Store", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //shareApplication(app);
                openPlayStore(app);
            }
        });*/

        dialog = alertDialog.show();
    }

    protected void openPlayStore(App app) {
        if (app == null || context == null) {
            return;
        }
        context.startActivity(new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://play.google.com/store/apps/details?id=" + app.packageName)));
    }


    private void unInstallApplication(App app) {
        Intent intent = new Intent(Intent.ACTION_DELETE, Uri.fromParts("package", app.getPackageName(), null));
        MyApplication.getCurrentActivityContext().startActivity(intent);
    }

    private void openApplicationSettings(App app) {
        try {
            //Open the specific App Info page:
            Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.setData(Uri.parse("package:" + app.getPackageName()));
            MyApplication.getCurrentActivityContext().startActivity(intent);

        } catch (ActivityNotFoundException e) {
            //e.printStackTrace();

            //Open the generic Apps page:
            Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
            MyApplication.getCurrentActivityContext().startActivity(intent);

        }
    }

    /*public void addBitmapToMemoryCache(String key, Drawable bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            cache.put(key, bitmap);
        }
    }*/

    /**
     * This will return a bitmap from the cache
     *
     * @return
     */
    /*public Drawable getBitmapFromMemCache(String key) {
        return cache.get(key);
    }*/


    public class MyImageLoadTask extends AsyncTask<String, Void, Drawable> {

        private ImageView imageView;
        // private PackageManager packageManager;


        public MyImageLoadTask(/*PackageManager manager,*/
                ImageView imageViewReference) {
            this.imageView = imageViewReference;
            // packageManager = manager;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //imageView.setBackground(null);
            // imageView.setImageDrawable(null);
            if (imageView != null)
                imageView.setImageResource(R.drawable.loading);
            //Picasso.with(context).load(R.drawable.loading).into(imageView);


        }

        /*
         * Loading the drawable icons (non-Javadoc)
         *
         * @see android.os.AsyncTask#doInBackground(Params[])
         */
        @Override
        protected Drawable doInBackground(String... params) {
            // App application = params[0];
            // ApplicationInfo info;
            try {
                String packageName = params[0];
                if (TextUtils.isEmpty(packageName)) {
                    return null;
                }

                Drawable loadIcon = context.getPackageManager().getApplicationInfo(packageName, 0).loadIcon(context.getPackageManager());
                //return context.getPackageManager().getApplicationIcon(packageName);

                // Drawable loadIcon = info.loadIcon(packageManager);

               /* info = packageManager.getApplicationInfo(packageName, 0);
                Drawable loadIcon = info.loadIcon(packageManager);*/

                // addBitmapToMemoryCache(getKeyValue(packageName), loadIcon);
                return loadIcon;
            } catch (Exception e) {
                e.printStackTrace();
            }
            // info = null;
            return null;
        }


        /*
         * Setting drawable to imageview
         *
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        @Override
        protected void onPostExecute(Drawable result) {
            super.onPostExecute(result);
            //imageView.setBackground(null);
            // imageView.setImageDrawable(null);
            try {
                if (result != null) {
                    if (imageView != null) {
                        imageView.setImageDrawable(result);
                    }
                    // orderList.get(position).drawable = result;
                    /*if(app!=null) {
                        app.drawable = result;
                    }*/


                } else {
                    if (imageView != null) {
                        imageView.setImageResource(R.drawable.loading);
                    }
                    //Picasso.with(context).load(R.drawable.loading).into(imageView);
                }
                /*if (position != -1 ) {
                notifyItemChanged(position);
                    notifyItemChanged(0);
                // notifyItemChanged(0);
                //notifyItemChanged(1);
                }else
                {
                    notifyDataSetChanged();
                }*/
            } catch (Exception e) {
            }

        }

    }


    private String getKeyValue(String name) {

        //name = name.replace("-", "").replace(" ", "").toLowerCase().trim();

        return name;//.substring(name.length() / 2, name.length());
            /*byte[] encodedKey     = Base64.decode(name, Base64.DEFAULT);
            SecretKey originalKey = new SecretKeySpec(encodedKey, 0, encodedKey.length/2, "AES");
            return originalKey.toString();*/
    }

    public class CustomFilter extends Filter {
        private AppListAdapter mAdapter;

        private CustomFilter(AppListAdapter mAdapter) {
            super();
            this.mAdapter = mAdapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            ArrayList<App> newList = new ArrayList<>();
            //orderList.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                newList.addAll(originalList);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final App mWords : originalList) {
                    if (mWords.getAppName().toLowerCase().contains(filterPattern)) {
                        newList.add(mWords);
                    }
                }
            }
            //System.out.println("Count Number " + orderList.size());
            /*if(orderList.size()>1) {
                orderList.remove(orderList.size() - 1);
            }*/
            //orderList=newList;
            results.values = newList;
            results.count = newList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            // System.out.println("Count Number 2 " + ((List<App>) results.values).size());
            if (objListener != null) {
                objListener.applicationsCount(results.count);
            }

           /* orderList.clear();
            ArrayList<App> applications = new ArrayList<>();
            App app= new App(true);
            app.appName="#";
            applications.add(0, app);
            applications.addAll((ArrayList<App>) results.values);*/

            orderList = (ArrayList<App>) results.values;
            notifyDataSetChanged();
            /*new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //notifyItemChanged(0);
                    notifyDataSetChanged();
                }
            }, 1500);*/
        }
    }

    public void hideKeyBord(View view) {
        if (view != null) {
            if (keyBoardHide == null) {
                keyBoardHide = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                // keyBoardHide.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY,
                // 0);
            }
            if (keyBoardHide != null && keyBoardHide.isActive()) {
                // to hide keyboard
                if (view != null) {
                    keyBoardHide.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
        }
    }


    // private List<String> mDataArray;
    public ArrayList<App> getOrdersList() {
        return orderList;
    }

    public boolean isSearchResult() {
        return orderList.size() != orderList.size();
    }

    private ArrayList<Integer> mSectionPositions;

    @Override
    public Object[] getSections() {
        synchronized (this) {
            List<String> sections = new ArrayList<>();


            mSectionPositions = new ArrayList<>();
            for (int i = 0, size = getOrdersList().size(); i < size; i++) {
                String section = String.valueOf(getOrdersList().get(i).appName.charAt(0)).toUpperCase();
                if (!sections.contains(section)) {
                    sections.add(section);
                    mSectionPositions.add(i);
                }
            }
            return sections.toArray(new String[]{});
        }

    }


    public Object[] getSections_old() {


        String[] stringArry = new String[]{"#", "A", "B", "C", "D", "F", "E", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};


      /*  if(true)
        {
            return stringArry;
        }*/
        //List<String> sections = new ArrayList<>(27);


        mSectionPositions = new ArrayList<>(27);
        for (int i = 0; i < stringArry.length; i++) {
            mSectionPositions.add(i);
            //String section = stringArry[i];  //String.valueOf(getOrdersList().get(i).appName.charAt(0)).toUpperCase();
            // if (!sections.contains(section)) {
            // sections.add(section);
            // mSectionPositions.add(i);
            //}
        }
        return stringArry;//sections.toArray(new String[0]);

    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return mSectionPositions.get(sectionIndex);
    }

    @Override
    public int getSectionForPosition(int position) {
        return 0;
    }


    private float convertDPToPixels(float dp) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float logicalDensity = metrics.density;
        return dp * logicalDensity;
    }

/*

    class PassThroughModelLoader<T> implements ModelLoader<T, T> {
        @Override public DataFetcher<T> getResourceFetcher(final T model, int width, int height) {
            return new DataFetcher<T>() {
                @Override public T loadData(Priority priority) throws Exception { return model; }
                @Override public void cleanup() { }
                @Override public String getId() { return "PassThroughDataFetcher"; }
                @Override public void cancel() { }
            };
        }
    }

    class ApplicationIconDecoder implements ResourceDecoder<ApplicationInfo, Drawable> {
        private final Context context;
        public ApplicationIconDecoder(Context context) { this.context = context; }
        @Override public Resource<Drawable> decode(ApplicationInfo source, int width, int height) throws IOException {
            Drawable icon = context.getPackageManager().getApplicationIcon(source);
            return new DrawableResource<Drawable>(icon) {
                @Override public int getSize() { // best effort
                    if (drawable instanceof BitmapDrawable) {
                        return Util.getBitmapByteSize(((BitmapDrawable)drawable).getBitmap());
                    } else {
                        return 1;
                    }
                }
                @Override public void recycle() { */
    /* not from our pool *//*
 }
            };
        }
        @Override public String getId() { return "ApplicationInfoToDrawable"; }
    }
*/


    /*class PassThroughModelLoader<T> implements ModelLoader<T, T> {
        @Override
        public DataFetcher<T> getResourceFetcher(final T model, int width, int height) {
            return new DataFetcher<T>() {
                @Override public T loadData(Priority priority) throws Exception { return model; }
                @Override public void cleanup() { }
                @Override public String getId() { return "PassThroughDataFetcher"; }
                @Override public void cancel() { }
            };
        }
    }*/

    /* class ApplicationIconDecoder implements ResourceDecoder<ApplicationInfo, Drawable> {
         private final Context context;
         public ApplicationIconDecoder(Context context) { this.context = context; }
         @Override
         public Resource<Drawable> decode(ApplicationInfo source, int width, int height) throws IOException {
             Drawable icon = context.getPackageManager().getApplicationIcon(source);
             return new DrawableResource<Drawable>(icon) {
                 @Override
                 public int getSize() { // best effort
                     if (drawable instanceof BitmapDrawable) {
                         return Util.getBitmapByteSize(((BitmapDrawable)drawable).getBitmap());
                     } else {
                         return 1;
                     }
                 }
                 @Override public void recycle() { *//* not from our pool *//* }
            };
        }
        @Override
        public String getId() { return "ApplicationInfoToDrawable"; }
    }
*/
    public boolean isHeader(int position) {

        boolean isEnable = SharedPreferenceUtils.getBooleanValueFromSharedPrefarence(Constants.Pref.IS_FREQ_USED_APPS_ENABLE, true);


        if (!isEnable) {
            return false;
        }

        return orderList.get(position).isHeader; //position == 0;
    }

    public void setIconSize(int icon_size1) {
        this.icon_size = icon_size1 * 10;
        resourceId = context.getResources().
                getIdentifier("_" + icon_size + "sp", "dimen", context.getPackageName());
        finalIconSize = (int) context.getResources().getDimension(resourceId);
        notifyDataSetChanged();
    }

}


