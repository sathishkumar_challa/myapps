package com.sparsh.miapps.adapters;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;

import com.sparsh.miapps.R;
import com.sparsh.miapps.application.MyApplication;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import com.sparsh.miapps.utils.Constants;
import com.sparsh.miapps.utils.SharedPreferenceUtils;
import com.sparsh.miapps.utils.Utils;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
/*import com.amazon.device.ads.Ad;
import com.amazon.device.ads.AdError;
import com.amazon.device.ads.AdLayout;
import com.amazon.device.ads.AdListener;
import com.amazon.device.ads.AdProperties;
import com.amazon.device.ads.AdTargetingOptions;
import com.amazon.device.ads.DefaultAdListener;*/

/**
 * Created by Sathish Kumar Challa on 28/10/18.
 */
public class SectionedGridRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements SectionIndexer {

    private final Context mContext;
    private static final int SECTION_TYPE = 0;

    private boolean mValid = true;
    private int mSectionResourceId;
    //private int mTextResourceId;
    private LayoutInflater mLayoutInflater;
    private RecyclerView.Adapter mBaseAdapter;
    private SparseArray<Section> mSections = new SparseArray<Section>();
    private RecyclerView mRecyclerView;


    public SectionedGridRecyclerViewAdapter(int sectionResourceId, RecyclerView recyclerView,
                                            RecyclerView.Adapter baseAdapter) {

        mLayoutInflater = (LayoutInflater) MyApplication.getCurrentActivityContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mSectionResourceId = sectionResourceId;
        mBaseAdapter = baseAdapter;
        mContext = MyApplication.getCurrentActivityContext();
        mRecyclerView = recyclerView;

        mBaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                mValid = mBaseAdapter.getItemCount() > 0;
                notifyDataSetChanged();
            }

            @Override
            public void onItemRangeChanged(int positionStart, int itemCount) {
                mValid = mBaseAdapter.getItemCount() > 0;
                notifyItemRangeChanged(positionStart, itemCount);
            }

            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                mValid = mBaseAdapter.getItemCount() > 0;
                notifyItemRangeInserted(positionStart, itemCount);
            }

            @Override
            public void onItemRangeRemoved(int positionStart, int itemCount) {
                mValid = mBaseAdapter.getItemCount() > 0;
                notifyItemRangeRemoved(positionStart, itemCount);
            }
        });

        final GridLayoutManager layoutManager = (GridLayoutManager) (mRecyclerView.getLayoutManager());
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return (isSectionHeaderPosition(position)) ? layoutManager.getSpanCount() : 1;
            }
        });
    }


    public static class SectionViewHolder extends RecyclerView.ViewHolder {
        // public TextView title;
        // private AdLayout adView;

        public SectionViewHolder(View view) {
            super(view);
            ((LinearLayout) view.findViewById(R.id.adds_linear)).removeAllViews();

           /* if(!SharedPreferenceUtils.getBooleanValueFromSharedPrefarence(AbstractFragmentActivity.is_display_banner_add,true))
            {
                return;
            }*/
            /*
                 SharedPreferenceUtils.setBooleanValueToSharedPrefarence(is_display_banner_add,is_display_banner);
        SharedPreferenceUtils.setBooleanValueToSharedPrefarence(is_display_intestrial_add,is_display_intestrial);
        SharedPreferenceUtils.setBooleanValueToSharedPrefarence(is_display_reward_add,is_display_reward);

            if (true) {
                return;
            }*/

            if (Utils.getDeviceId().equalsIgnoreCase(Constants.DEVICE_ID_B)) {
                return;
            }


           /* this.adView =view.findViewById(R.id.adView);
            adView.setTimeout(20000); // 20 seconds


            AdTargetingOptions adOptions = new AdTargetingOptions();
            // Optional: Set ad targeting options here.
            adOptions.enableGeoLocation(true);
           // adOptions.setAdvancedOption(AdLayout.)
            this.adView.loadAd(adOptions); // Retrieves an ad on background thread

            adView.setListener(new DefaultAdListener());
*/
/*adView.setListener(new AdListener() {
                @Override
                public void onAdLoaded(Ad ad, AdProperties adProperties) {

                }

                @Override
                public void onAdFailedToLoad(Ad ad, AdError adError) {

                }

                @Override
                public void onAdExpanded(Ad ad) {

                }

                @Override
                public void onAdCollapsed(Ad ad) {

                }

                @Override
                public void onAdDismissed(Ad ad) {

                }
            });*//*

            new android.os.Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(adView!=null && adView.loadAd()) {
                        adView.showAd();
                    }
                }
            },2000);

           if( MyApplication.getCurrentActivityContext() instanceof AbstractMenuActivity)
           {
               AbstractMenuActivity activity= (AbstractMenuActivity) MyApplication.getCurrentActivityContext();

               activity.setOnMyAdsListener(new AbstractMenuActivity.MyAdsListener() {
                   @Override
                   public void onDestroyClick() {
                       if(adView!=null ) {
                           adView.destroy();
                       }
                   }
               });
           }
*/

            //  mAdView.setAdUnitId(AD_UNIT_ID);

            // mAdView = view.findViewById(R.id.adView);





        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int typeView) {
        if (typeView == SECTION_TYPE) {
            final View view = LayoutInflater.from(mContext).inflate(mSectionResourceId, parent, false);
            return new SectionViewHolder(view);
        } else {
            return mBaseAdapter.onCreateViewHolder(parent, typeView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder sectionViewHolder, int position) {
        if (isSectionHeaderPosition(position) || (position == (mBaseAdapter.getItemCount()))) {
            // ((SectionViewHolder)sectionViewHolder).title.setText(mSections.get(position).title);

        } else {
            mBaseAdapter.onBindViewHolder(sectionViewHolder, sectionedPositionToPosition(position));
        }

    }

    @Override
    public int getItemViewType(int position) {
        return /*mBaseAdapter.getItemCount()==position*/isSectionHeaderPosition(position)
               /* position==0*/
                ? SECTION_TYPE
                : 1;//mBaseAdapter.getItemViewType(sectionedPositionToPosition(position)) + 1;
    }


    public static class Section {
        int firstPosition;
        int sectionedPosition;
        CharSequence title;

        public Section(int firstPosition, CharSequence title) {
            this.firstPosition = firstPosition;
            this.title = title;
        }

        public CharSequence getTitle() {
            return title;
        }
    }


    public void setSections(Section[] sections) {
        mSections.clear();

        Arrays.sort(sections, new Comparator<Section>() {
            @Override
            public int compare(Section o, Section o1) {
                return (o.firstPosition == o1.firstPosition)
                        ? 0
                        : ((o.firstPosition < o1.firstPosition) ? -1 : 1);
            }
        });

        int offset = 0; // offset positions for the headers we're adding
        for (Section section : sections) {
            section.sectionedPosition = section.firstPosition + offset;
            mSections.append(section.sectionedPosition, section);
            ++offset;
        }

        notifyDataSetChanged();
    }

    /*public int positionToSectionedPosition(int position) {
        int offset = 0;
        for (int i = 0; i < mSections.size(); i++) {
            if (mSections.valueAt(i).firstPosition > position) {
                break;
            }
            ++offset;
        }
        return position + offset;
    }*/

    public int sectionedPositionToPosition(int sectionedPosition) {
        if (isSectionHeaderPosition(sectionedPosition)) {
            return RecyclerView.NO_POSITION;
        }

        int offset = 0;
        for (int i = 0; i < mSections.size(); i++) {
            if (mSections.valueAt(i).sectionedPosition > sectionedPosition) {
                break;
            }
            --offset;
        }
        return sectionedPosition + offset;
    }

    public boolean isSectionHeaderPosition(int position) {
        return mSections.get(position) != null;//position==0;/*mBaseAdapter.getItemCount() == position;*///mSections.get(position) != null;
    }


    @Override
    public long getItemId(int position) {
        return isSectionHeaderPosition(position)
                ? Integer.MAX_VALUE - mSections.indexOfKey(position)
                : mBaseAdapter.getItemId(sectionedPositionToPosition(position));
    }

    @Override
    public int getItemCount() {
        //return (mValid ? mBaseAdapter.getItemCount() + mSections.size() : 0);
       /* if (mBaseAdapter.getItemCount() <= 3) {
            return mBaseAdapter.getItemCount();
        }*/
        return (mValid ? mBaseAdapter.getItemCount() + mSections.size() : 1);
    }


    @Override
    public int getSectionForPosition(int position) {
        return position;
    }

    private ArrayList<Integer> mSectionPositions;


    @Override
    public Object[] getSections() {
        List<String> sections = new ArrayList<>(27);


        mSectionPositions = new ArrayList<>(27);
        for (int i = 0, size = ((AppListAdapter) mBaseAdapter).getOrdersList().size(); i < size; i++) {
            String section = String.valueOf(((AppListAdapter) mBaseAdapter).getOrdersList().get(i).appName.charAt(0)).toUpperCase();
            if (!sections.contains(section)) {
                sections.add(section);
                mSectionPositions.add(i);
            }
        }
        return sections.toArray(new String[0]);

    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        if (mSectionPositions.size() == 0) {
            return 0;
        }
        return mSectionPositions.get(sectionIndex);
    }


   /* public void closeAds()
    {
        if(adView!=null) {
            this.adView.destroy();
        }
    }*/


}
