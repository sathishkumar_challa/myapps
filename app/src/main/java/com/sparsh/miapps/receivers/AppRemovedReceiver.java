package com.sparsh.miapps.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.sparsh.miapps.application.MyApplication;
import com.sparsh.miapps.core.App;
import com.sparsh.miapps.database.ApplicationsDatabase;
import com.sparsh.miapps.utils.Constants;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;


public class AppRemovedReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent == null) {
            return;
        }
        ApplicationsDatabase db = new ApplicationsDatabase(context);
        try {
            if (intent != null) {
                String unInstallPackage = intent.getData()
                        .getSchemeSpecificPart();

                if (unInstallPackage != null) {
                    App application = new App(false);
                    application.packageName = unInstallPackage;
                    db.removeApplication(application);
                    if (MyApplication.getCurrentActivityContext() != null) {
                        LocalBroadcastManager.getInstance(MyApplication.getCurrentActivityContext()).sendBroadcast(
                                new Intent(Constants.MY_INTENT_FILTER));
                    }
                }
            }
        } catch (Exception e) {
        }
    }
}
