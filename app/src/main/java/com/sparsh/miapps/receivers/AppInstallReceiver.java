package com.sparsh.miapps.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;

import com.sparsh.miapps.application.MyApplication;
import com.sparsh.miapps.core.App;
import com.sparsh.miapps.database.ApplicationsDatabase;
import com.sparsh.miapps.utils.Constants;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;


public class AppInstallReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent == null) {
            return;
        }
        ApplicationsDatabase db = new ApplicationsDatabase(context);
        if (db.getCount() < 2) {
            return;
        }
        App application = new App(false);
        try {
            String installPackage = intent.getData().getSchemeSpecificPart();
            if (installPackage != null) {
                if (db.isSystemApplication(installPackage)) {
                    return;
                }
                application.packageName = installPackage;
                PackageInfo packageInfo = context.getPackageManager()
                        .getPackageInfo(installPackage, 0);
                CharSequence loadLabel = packageInfo.applicationInfo
                        .loadLabel(context.getPackageManager());
                application.appName = loadLabel.toString();


                boolean userApp = isUserApp(packageInfo.applicationInfo);
                application.isUserApp=userApp?1:0;


                db.addApplication(application);


                if (MyApplication.getCurrentActivityContext() != null) {
                    LocalBroadcastManager.getInstance(MyApplication.getCurrentActivityContext()).sendBroadcast(
                            new Intent(Constants.MY_INTENT_FILTER));
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    boolean isUserApp(ApplicationInfo ai) {
        int mask = ApplicationInfo.FLAG_SYSTEM
                | ApplicationInfo.FLAG_UPDATED_SYSTEM_APP;
        return (ai.flags & mask) == 0;
    }
}
