/*
 * Copyright (C) 2016 Ronald Martin <hello@itsronald.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Last modified 10/13/16 2:53 PM.
 */

package com.sparsh.miapps.activities;

import android.os.Bundle;
import android.os.Handler;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.sparsh.miapps.R;
import com.sparsh.miapps.application.MyApplication;
import com.sparsh.miapps.utils.Constants;
import com.sparsh.miapps.utils.SharedPreferenceUtils;
import com.sparsh.miapps.utils.Utils;
/*import com.sathishinfo.jkatelangana.R;
import com.sathishinfo.jkatelangana.application.MyApplication;
import com.sathishinfo.jkatelangana.utils.Constants;
import com.sathishinfo.jkatelangana.utils.SharedPreferenceUtils;
import com.sathishinfo.jkatelangana.utils.Utils;*/

public class GoogleAdActivity extends BaseActivity {


    private InterstitialAd mInterstitialAd;
    private RewardedVideoAd mRewardedVideoAd;

    private boolean isFromBroadcast = false;
    public static final String IS_FROM_BROADCAST_KEY = "is_frombroad_cast_key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  setContentView(R.layout.activity_simple_java_example);
        //  flag=false;
        loadAdd();
        init();

        //isFromBroadcast= getIntent().getBooleanExtra(IS_FROM_BROADCAST_KEY,false);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mRewardedVideoAd != null) {
            mRewardedVideoAd.resume(this);
        }
        /*if(isFromBroadcast) {
            checkAndShowAdd();
        }else*/// {
       /* new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                checkAndShowAdd();
            }


        }, 2000);*/
        // }


    }

    private void loadAdd() {

        if (SharedPreferenceUtils.getBooleanValueFromSharedPrefarence(AbstractFragmentActivity.is_display_intestrial_add, true)) {

            if (!Utils.getDeviceId().equalsIgnoreCase(Constants.DEVICE_ID_B)) {
                if (Constants.IS_TESTING) {
                    loadInterstitialAdd("ca-app-pub-3940256099942544/1033173712");
                } else {
                    loadInterstitialAdd(Utils.getStringFromResources(R.string.google_add_Interstitial_key));
                }
            }
            //setRewardAd("ca-app-pub-8492614871633143/3007006445");

        } else if (SharedPreferenceUtils.getBooleanValueFromSharedPrefarence(AbstractFragmentActivity.is_display_reward_add, true)) {

            if (!Utils.getDeviceId().equalsIgnoreCase(Constants.DEVICE_ID_B)) {

                if (Constants.IS_TESTING) {
                    setRewardAd("ca-app-pub-3940256099942544/5224354917");
                } else {
                    setRewardAd(Utils.getStringFromResources(R.string.goolge_award_video_add_key));
                }
            }

        } else {

        }
    }

    private void loadInterstitialAdd(String interstitialId) {
        MobileAds.initialize(this, "ca-app-pub-8492614871633143~8209121764"/*"ca-app-pub-3940256099942544~3347511713"*/);
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(interstitialId);
        AdRequest build = null;//new AdRequest.Builder().build();

        String deviceId = Utils.getDeviceId();
        if (deviceId.equalsIgnoreCase("8ae13557eb6f13e9")) {
            build = new AdRequest.Builder().addTestDevice("65BB51B60C1F6209AA3A4D0BB6197FA2").build();

        } /*else if (deviceId.equalsIgnoreCase("6f0c172cceb4f3da")) {
            build = new AdRequest.Builder().addTestDevice("F425A6B5C599E397F4873E7C1C2E88F9").build();

        }*/ else {

            build = new AdRequest.Builder().build();

        }

        mInterstitialAd.loadAd(build);

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                loadAdd();

            }

            @Override
            public void onAdClicked() {
                super.onAdClicked();

            }
        });


    }

    private void setRewardAd(String adId) {
        MobileAds.initialize(this,
                "ca-app-pub-8492614871633143~8209121764");
        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        mRewardedVideoAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
            @Override
            public void onRewardedVideoAdLoaded() {

            }

            @Override
            public void onRewardedVideoAdOpened() {

            }

            @Override
            public void onRewardedVideoStarted() {

            }

            @Override
            public void onRewardedVideoAdClosed() {

                loadAdd();
            }

            @Override
            public void onRewarded(RewardItem rewardItem) {

            }

            @Override
            public void onRewardedVideoAdLeftApplication() {

            }

            @Override
            public void onRewardedVideoAdFailedToLoad(int i) {
                loadAdd();
            }

            @Override
            public void onRewardedVideoCompleted() {

            }

           /* @Override
            public void onRewardedVideoCompleted() {

            }*/
        });
        loadRewardedVideoAd(adId);
    }

    private void loadRewardedVideoAd(final String adId) {

        if (Utils.getDeviceId().equalsIgnoreCase(Constants.DEVICE_ID_B)) {

            return;
        }

        AdRequest build = null;//new AdRequest.Builder().build();

        String deviceId = Utils.getDeviceId();
        if (deviceId.equalsIgnoreCase("8ae13557eb6f13e9")) {
            build = new AdRequest.Builder().addTestDevice("65BB51B60C1F6209AA3A4D0BB6197FA2").build();

        }/*else if(deviceId.equalsIgnoreCase("6f0c172cceb4f3da")){
            build = new AdRequest.Builder().addTestDevice("F425A6B5C599E397F4873E7C1C2E88F9").build();

        }*/ else {

            build = new AdRequest.Builder().build();

        }

        mRewardedVideoAd.loadAd(adId/*"ca-app-pub-3940256099942544/5224354917"*/,
                build);

        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mRewardedVideoAd.isLoaded()) {
                    mRewardedVideoAd.show();
                }else
                {
                    loadRewardedVideoAd(adId);
                }
            }
        },DEFAULT_MINS*60*1000);*/
    }


    private boolean showVideoAd() {
        if (isFinishing()) {
            return false;
        }
        if (Utils.getDeviceId().equalsIgnoreCase(Constants.DEVICE_ID_B)) {

            return false;
        }
        if (mRewardedVideoAd != null && mRewardedVideoAd.isLoaded()) {
            mRewardedVideoAd.show();
            return true;
        } else {
            if (SharedPreferenceUtils.getBooleanValueFromSharedPrefarence(AbstractFragmentActivity.is_display_reward_add, true)) {


                if (Constants.IS_TESTING) {
                    setRewardAd("ca-app-pub-3940256099942544/5224354917");
                } else {
                    setRewardAd(Utils.getStringFromResources(R.string.goolge_award_video_add_key));
                }

            }
        }
        return false;

    }

    private boolean showAd() {
        if (isFinishing()) {
            return false;
        }
        if (Utils.getDeviceId().equalsIgnoreCase(Constants.DEVICE_ID_B)) {

            return false;
        }
        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
            return true;
        } else {
            if (SharedPreferenceUtils.getBooleanValueFromSharedPrefarence(AbstractFragmentActivity.is_display_intestrial_add, true)) {


                if (Constants.IS_TESTING) {
                    loadInterstitialAdd("ca-app-pub-3940256099942544/1033173712");
                    // showAd();
                } else {
                    loadInterstitialAdd(Utils.getStringFromResources(R.string.google_add_Interstitial_key));
                }
            }
        }
        return false;

    }

    protected boolean checkAndShowAdd() {
        boolean isShow = false;
        if (SharedPreferenceUtils.getBooleanValueFromSharedPrefarence(AbstractFragmentActivity.is_display_intestrial_add, false)) {


            if (showAd()) {
                isShow = true;
                MyApplication.showTransparentDialog1();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        MyApplication.hideTransaprentDialog1();
                        // moveTaskToBack(true);
                    }
                }, 2000);
            } else {

            }
        } else if (SharedPreferenceUtils.getBooleanValueFromSharedPrefarence(AbstractFragmentActivity.is_display_reward_add, false)) {
            if (showVideoAd()) {
                isShow = true;
                MyApplication.showTransparentDialog1();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        MyApplication.hideTransaprentDialog1();
                        // moveTaskToBack(true);
                    }
                }, 2000);
            } else {

            }
        } else {

        }
        return isShow;
    }


    private void init() {


    }


    @Override
    protected void onRestart() {
        super.onRestart();
        //flag=false;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mRewardedVideoAd != null) {
            mRewardedVideoAd.destroy(this);
        }
    }

    @Override
    public void onPause() {
        if (mRewardedVideoAd != null) {
            mRewardedVideoAd.pause(this);
        }
        super.onPause();
    }

}
