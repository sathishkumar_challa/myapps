package com.sparsh.miapps.activities;

import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.sparsh.miapps.R;
import com.sparsh.miapps.fragments.AbstractFragment;
import com.sparsh.miapps.views.IndexFastScrollRecyclerView;


/**
 * Created by sathishkumar on 9/6/16.
 */
abstract class SwipedRecyclerViewActivity extends AppCompatActivity {
    protected SwipeRefreshLayout swipeLayout;
    protected IndexFastScrollRecyclerView recyclerView;
    protected boolean loading = false;
    protected boolean isFirstLoading;



    protected void setupSwipeRefreshLayout() {
        swipeLayout = findViewById(R.id.swipe_container1);
        if (swipeLayout == null) {
            return;
        }
        // swipeLayout.setEnabled(false);
        // swipeLayout.setVisibility(View.VISIBLE);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {

                //swipeLayout.setRefreshing(false);

                onSwipeLayoutSetToDefaultPage();
                return;


            }

        });
        swipeLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    /*protected void showNoRecordFoundTextView(boolean isShow) {
        if (noRecordsFoundRelative == null) {
            return;
        }
        noRecordsFoundRelative.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }*/

    protected void addListenersToRecyclerView() {
        if (recyclerView == null) {
            return;
        }
        isFirstLoading=true;

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

       /* recyclerView.addOnScrollListener(new OnVerticalScrollListener() {

            @Override
            public void onScrolledToEnd() {
                if(isFirstLoading)
                {
                    isFirstLoading=false;
                    return;
                }
                recylerViewScrollListener();
            }

            @Override
            public void onScrolledUp() {
                super.onScrolledUp();
                recylerViewScrollListener();
            }
        });*/

    }

     protected void onSwipeLayoutSetToDefaultPage(){

     }

    //abstract  protected void recylerViewScrollListener();

}