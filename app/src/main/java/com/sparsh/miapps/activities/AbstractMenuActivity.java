package com.sparsh.miapps.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.FirebaseApp;
import com.sparsh.miapps.R;
import com.sparsh.miapps.application.MyApplication;
import com.sparsh.miapps.colorpicker.ColorActivity;
import com.sparsh.miapps.fragments.AbstractFragment;
import com.sparsh.miapps.fragments.HomeFragment;
import com.sparsh.miapps.interfaces.MyTitleBarMenuListener;
import com.sparsh.miapps.interfaces.OnSeekbarChangeListener;
import com.sparsh.miapps.interfaces.OnSeekbarFinalValueListener;
import com.sparsh.miapps.utils.Constants;
import com.sparsh.miapps.utils.SharedPreferenceUtils;
import com.sparsh.miapps.utils.Utils;
/*import com.sparsh.miapps.views.colorPicker.ColorEnvelope;
import com.sparsh.miapps.views.colorPicker.ColorListener;
import com.sparsh.miapps.views.colorPicker.ColorPickerDialog;
import com.sparsh.miapps.views.colorPicker.ColorPickerView;
import com.sparsh.miapps.views.colorPicker.CustomFlag;*/
import com.sparsh.miapps.widgets.CrystalSeekbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import static com.sparsh.miapps.activities.AbstractFragmentActivity.actual_clicks_key;
import static com.sparsh.miapps.activities.AbstractFragmentActivity.intestrial_ad_delay_clicks;
import static com.sparsh.miapps.activities.AbstractFragmentActivity.is_display_side_menu_pro;
import static com.sparsh.miapps.utils.Utils.showToastMessage;



//import com.amazon.device.ads.AdRegistration;
/*import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;*/
/*
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
*/
//import com.google.android.gms.ads.InterstitialAd;
/*
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardedVideoAd;
*/

/**
 * Created by Satishk on 8/21/2017.
 */

public abstract class AbstractMenuActivity extends GoogleAdActivity implements View.OnClickListener/*, com.jaredrummler.android.colorpicker.ColorPickerView.OnColorChangedListener*/ {
    private InputMethodManager keyBoardHide;

    private DrawerLayout drawerLayout;
    //private TextView titleTextview;
    private LinearLayout linearLayout;
    private ActionBarDrawerToggle drawerToggle;

    private MyAdsListener adsListener1;
    private RelativeLayout toolbarRelative, main_content_relative;

    //  private ColorPickerView colorPickerView_old;

    private LinearLayout  search_view_linear;

    protected TextView title_logo_tv;
    protected  ImageView menuIconRight,search_imageView;
    protected TextView count_tv;

    //private com.jaredrummler.android.colorpicker.ColorPickerView colorPickerView;
    //private ColorPanelView newColorPanelView;


    // private InterstitialAd mInterstitialAd;

    //private final int DEFAULT_MINS=5;


    // private int mins=DEFAULT_MINS;
    // private Handler handler;

    //private RewardedVideoAd mRewardedVideoAd;


    // private AbstractFragment currentFragment;/*, prevoiusFragment;*/


    // private  Intent  intent;

    private int MIN_VALUE = 2;
    //rivate float MAX_VALUE = 10000;


    //private CrystalSeekbar rangeSeekbar;
    private View spancount_view,icon_size_view;

    protected HomeFragment homeFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkNetwork();
        try {
            FirebaseApp.initializeApp(this);
        } catch (Exception e) {

        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        setColorToTextView(SharedPreferenceUtils.getIntValueToSharedPrefarence(getResources().getString(R.string.ToolbarColorPickerPreference), getColorCode(R.color.colorPrimary)), toolbarRelative);
        setColorToTextView(SharedPreferenceUtils.getIntValueToSharedPrefarence(getResources().getString(R.string.BackgroundColorPickerPreference), getColorCode(R.color.normal_bg)), main_content_relative);
        setColorToTextView(SharedPreferenceUtils.getIntValueToSharedPrefarence(getResources().getString(R.string.BackgroundColorPickerPreference), getColorCode(R.color.normal_bg)), search_view_linear);

        setTitleNameColor();
        setProVertionViewinSideMenu();

        boolean isEnable= SharedPreferenceUtils.getBooleanValueFromSharedPrefarence(Constants.Pref.IS_FREQ_USED_APPS_ENABLE,true);

        SwitchCompat switchCompat= findViewById(R.id.freq_used_apps_switch);
        switchCompat.setChecked(isEnable);

        boolean isShoeTitleEnable= SharedPreferenceUtils.getBooleanValueFromSharedPrefarence(Constants.Pref.IS_SHOW_TITLE_BAR_ENABLE,true);

        SwitchCompat titleSwitchCompat= findViewById(R.id.show_title_bar_switch);
        titleSwitchCompat.setChecked(isShoeTitleEnable);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setTitleNameColor();
            }
        },300);


    }

    protected void setTitleNameColor() {

        if(title_logo_tv!=null) {
            title_logo_tv.setTextColor(SharedPreferenceUtils.getIntValueToSharedPrefarence(getResources().getString(R.string.title_color), getColorCode(R.color.white)));
        }

        if(count_tv!=null) {
            count_tv.setTextColor(SharedPreferenceUtils.getIntValueToSharedPrefarence(getResources().getString(R.string.title_color), getColorCode(R.color.white)));
        }



        if (menuIconRight !=null) {
            menuIconRight.getDrawable().setColorFilter(SharedPreferenceUtils.getIntValueToSharedPrefarence(getResources().getString(R.string.title_color), getColorCode(R.color.white)), PorterDuff.Mode.SRC_ATOP );
        }else
        {
            menuIconRight = findViewById(R.id.refresh_imageView);
            if (menuIconRight !=null) {
                menuIconRight.getDrawable().setColorFilter(SharedPreferenceUtils.getIntValueToSharedPrefarence(getResources().getString(R.string.title_color), getColorCode(R.color.white)), PorterDuff.Mode.SRC_ATOP );
            }
        }


        if (search_imageView !=null) {
            search_imageView.getDrawable().setColorFilter(SharedPreferenceUtils.getIntValueToSharedPrefarence(getResources().getString(R.string.title_color), getColorCode(R.color.white)), PorterDuff.Mode.SRC_ATOP );
        }else
        {
            search_imageView  = findViewById(R.id.search_imageView);
            if (search_imageView !=null) {
                search_imageView.getDrawable().setColorFilter(SharedPreferenceUtils.getIntValueToSharedPrefarence(getResources().getString(R.string.title_color), getColorCode(R.color.white)), PorterDuff.Mode.SRC_ATOP );
            }
        }




        //setColorToTextView(SharedPreferenceUtils.getIntValueToSharedPrefarence(getResources().getString(R.string.title_color), getColorCode(R.color.white)), title_logo_tv);


    }

    protected void setTitleBarName(String str) {
        if (getSupportActionBar() != null && !TextUtils.isEmpty(str)) {
            getSupportActionBar().setTitle(str);
        }
    }


    protected void checkNetwork() {
       /* if (!Utils.isNetworkAvailable(this)) {

            showAlertDialog("Alert", "Network not available", new MyDialogInterface() {
                @Override
                public void onPossitiveClick() {
                    checkNetwork();
                }

                @Override
                public void onNegetiveClick() {
                    finish();
                }

            });
            return;
        }*/
        setMyContectntView();
        init();
        onColorPickerSetup();
    }

    protected void setMyContectntView() {
        setContentView(R.layout.activity_menu_layout);
        setupSwipeRefreshLayout();


    }


    protected void init() {


        spancount_view = findViewById(R.id.spancount_view);
        icon_size_view  = findViewById(R.id.icon_size_view);

        spancount_view.setVisibility(View.GONE);
        icon_size_view.setVisibility(View.GONE);


        /*color_picker_title = findViewById(R.id.color_picker_title);
       // colorPickerView_old = findViewById(R.id.colorPickerView);

        cancel_tv = findViewById(R.id.cancel_tv);

        confirm_tv = findViewById(R.id.confirm_tv);
        color_picker_linear = findViewById(R.id.color_picker_linear);
        color_picker_linear.setVisibility(View.GONE);*/

        toolbarRelative = findViewById(R.id.toolbar_relative);
        main_content_relative = findViewById(R.id.main_content_relative);

        search_view_linear = findViewById(R.id.search_view_linear);

        drawerLayout = findViewById(R.id.drawer_layout);
        drawerLayout.setScrimColor(Color.TRANSPARENT);
        linearLayout = (LinearLayout) findViewById(R.id.left_drawer);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBord(v);
                drawerLayout.closeDrawers();
            }
        });


      /*  Menu nav_Menu = drawerLayout.getMenu();
        nav_Menu.findItem(R.id.nav_settings).setVisible(false);*/


        // if(nameTv==null) {
        //}
        //titleRightImageview=findViewById(R.id.title_right_iv);


        setFragment();
        setSideMenuLayout();
        setSideMenuCutomerTitle();
        setActionBar();


    }


    private void setSideMenuLayout() {

        callCategoriesMapper();
        setSidemenuItemsListener();
    }



   /* private ObjectAnimator createRotateAnimator(final View target, final float from, final float to) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target, "rotation", from, to);
        animator.setDuration(300);
        animator.setInterpolator(com.github.aakira.expandablelayout.Utils.createInterpolator(com.github.aakira.expandablelayout.Utils.LINEAR_INTERPOLATOR));
        return animator;
    }*/

    protected abstract void setActionBar();


    public void setSidemenuItemsListener() {
        findViewById(R.id.like_app_side_menu).setOnClickListener(this);
        findViewById(R.id.share_app_side_menu).setOnClickListener(this);


        findViewById(R.id.set_span_count_linear).setOnClickListener(this);
        findViewById(R.id.set_app_icon_size_linear).setOnClickListener(this);
        findViewById(R.id.edit_title_name_linear).setOnClickListener(this);

        findViewById(R.id.pro_ver_linear).setOnClickListener(this);
        findViewById(R.id.terms_linear).setOnClickListener(this);
        findViewById(R.id.pref_color_linear).setOnClickListener(this);

        findViewById(R.id.freq_used_apps_linear).setOnClickListener(this);
        findViewById(R.id.freq_used_apps_switch).setOnClickListener(this);

        findViewById(R.id.show_title_bar_linear).setOnClickListener(this);
        findViewById(R.id.show_title_bar_switch).setOnClickListener(this);

        findViewById(R.id.a_to_z_linear).setOnClickListener(this);
        findViewById(R.id.z_to_a_linear).setOnClickListener(this);
        findViewById(R.id.refresh_linear).setOnClickListener(this);
        findViewById(R.id.reset_linear).setOnClickListener(this);





        ((TextView) findViewById(R.id.vertion_name_tv)).setText("Version : " + getVersionName());

        if (homeFragment != null) {
            homeFragment.setOnHomeFragmentListener(new HomeFragment.HomeFragmentListener() {
                @Override
                public void onRecylerViewClicked(View view) {
                   /* if (spancount_view != null) {

                        if (homeFragment != null) {
                            homeFragment.setSpanCount(SharedPreferenceUtils.getIntValueToSharedPrefarence(Constants.Pref.SPANCOUNT, 4));
                        }
                        spancount_view.setVisibility(View.GONE);
                    }*/

                   /* if (icon_size_view != null) {

                        *//*if (homeFragment != null) {
                            homeFragment.setIconSize(SharedPreferenceUtils.getIntValueToSharedPrefarence(Constants.Pref.APP_ICON_SIZE, 5));
                        }*//*
                        icon_size_view.setVisibility(View.GONE);
                    }*/


                  /*  if (color_picker_linear != null) {
                        color_picker_linear.setVisibility(View.GONE);
                        setColorToTextView(SharedPreferenceUtils.getIntValueToSharedPrefarence(getResources().getString(R.string.ToolbarColorPickerPreference), getColorCode(R.color.colorPrimary)), toolbarRelative);
                        setColorToTextView(SharedPreferenceUtils.getIntValueToSharedPrefarence(getResources().getString(R.string.BackgroundColorPickerPreference), getColorCode(R.color.normal_bg)), main_content_relative);

                    }*/
                }
            });
        }
        /*   findViewById(R.id.home_side_menu).setOnClickListener(this);
        findViewById(R.id.about_us_side_menu).setOnClickListener(this);
        findViewById(R.id.history_side_menu).setOnClickListener(this);
        findViewById(R.id.gallery_side_menu).setOnClickListener(this);
        findViewById(R.id.jka_masters_side_menu).setOnClickListener(this);
        findViewById(R.id.news_side_menu).setOnClickListener(this);
        findViewById(R.id.contact_info_side_menu).setOnClickListener(this);
        findViewById(R.id.other_zone_side_menu).setOnClickListener(this);
        findViewById(R.id.like_app_side_menu).setOnClickListener(this);
        findViewById(R.id.share_app_side_menu).setOnClickListener(this);
*/

    }

    @Override
    public void onClick(final View item) {
        hideKeyBord(item);
        //  Intent intent = getIntent();
        // Log.i("Menu1", "Clicked");
        closeMenu();
        MyApplication.hideTransaprentDialog1();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                MyApplication.hideTransaprentDialog1();
                if (item != null) {
                    openScreen(item);
                }

            }
        }, 250);

        /*switch (item.getId()) {
            case R.id.okButton:
                SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this).edit();
                edit.putInt("color_3", colorPickerView.getColor());
                edit.apply();
                finish();
                break;
            case R.id.cancelButton:
                finish();
                break;
        }*/


        // closeMenu();
    }

    public void closeDialogViews() {
        if (spancount_view != null) {
            spancount_view.setVisibility(View.GONE);
            hideKeyBord(spancount_view);
        }

        if (icon_size_view != null) {
            icon_size_view.setVisibility(View.GONE);
            hideKeyBord(icon_size_view);
        }

        /*if (color_picker_linear != null) {
            color_picker_linear.setVisibility(View.GONE);
            hideKeyBord(color_picker_linear);
        }*/
    }


    private void openScreen(@NonNull View item) {
        hideKeyBord(item);
        if (spancount_view != null) {
            spancount_view.setVisibility(View.GONE);
        }
        if (icon_size_view != null) {
            icon_size_view.setVisibility(View.GONE);
        }

        /*if (color_picker_linear != null) {
            color_picker_linear.setVisibility(View.GONE);
        }*/
        closeMenu();
        hideKeyBord(item);
        MyTitleBarMenuListener menuListener = getMyTitleBarMenuListener();
        switch (item.getId()) {
          /*  case R.id.home_side_menu:
                setFragment(new HomeFragment());
                setTitleBarName("JKA Telangana");
                break;
            case R.id.about_us_side_menu:
                setFragment(new AboutUsFragment());
                setTitleBarName("About Us");
                break;
            case R.id.history_side_menu:
                setFragment(new HistoryFragment());
                setTitleBarName("History");
                break;
            case R.id.gallery_side_menu:
                setFragment(new GalleryFragment());
                setTitleBarName("Gallery");
                break;
            case R.id.jka_masters_side_menu:
                setFragment(new JKAMastersFragment());
                setTitleBarName("JKA Masters");
                break;
            case R.id.news_side_menu:
                setFragment(new NewsFragment());
                setTitleBarName("Latest News");
                break;
            case R.id.contact_info_side_menu:
                setFragment(new ContactInfoFragment());
                setTitleBarName("Contact Info");
                break;
            case R.id.other_zone_side_menu:
                setFragment(new OtherZoneFragment());
                setTitleBarName("Other Zone");
                break;*/

            case R.id.set_app_icon_size_linear:
                if (icon_size_view != null) {
                    icon_size_view.setVisibility(View.VISIBLE);
                    setAppIconSizeRageSeekBar();
                }
                break;


            case R.id.set_span_count_linear:

                if (spancount_view != null) {
                    spancount_view.setVisibility(View.VISIBLE);
                    setAmountRageSeekBar();
                }
                // setToolBarColor(R.string.BackgroundColorPickerPreference, "Background Color", true);
                //showToastMessage("Coming soon");
                break;
 /*           findViewById(R.id.pro_ver_linear).setOnClickListener(this);
            findViewById(R.id.terms_linear).setOnClickListener(this);
            findViewById(R.id.pref_color_linear).setOnClickListener(this);
*/
            case R.id.pref_color_linear:
                if(true)
                {
                    Intent intent= new Intent(AbstractMenuActivity.this, ColorActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP|Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                    break;
                }

                // setToolBarColor(R.string.BackgroundColorPickerPreference, "Background Color", true);
                //showToastMessage("Coming soon");
                break;

            case R.id.terms_linear:
                /*if(true)
                {
                    startActivity(new Intent(AbstractMenuActivity.this, ColorActivity.class));
                    break;
                }
                setToolBarColor(R.string.ToolbarColorPickerPreference, "Toolbar Color", false);
 */
                termsAndConditions();
                //showToastMessage("Terms & Conditions");
                break;
            case R.id.pro_ver_linear:
                showToastMessage("Coming soon");
                break;

            case R.id.like_app_side_menu:
                rateIt();
                //showToastMessage("Coming soon");
                break;

            case R.id.share_app_side_menu:
                //showToastMessage("Coming soon");
                shareApp();
                break;

            case R.id.edit_title_name_linear:
                //showToastMessage("Coming soon");
                showTitleAletrDialog();
                break;

            case R.id.freq_used_apps_linear:
                //showToastMessage("Coming soon");
                frequentlyUsedAppsSwitchclick();
                break;


            case R.id.freq_used_apps_switch:
                //showToastMessage("Coming soon");
                frequentlyUsedAppsSwitchclick();
                break;

            case R.id.show_title_bar_linear:
                //showToastMessage("Coming soon");
                showTitleBarSwitchclick();
                break;

            case R.id.show_title_bar_switch:
                //showToastMessage("Coming soon");
                showTitleBarSwitchclick();
                break;

            case R.id.a_to_z_linear:
                //showToastMessage("Coming soon");

                if(menuListener!=null) {
                    menuListener.actionAtoZClick();
                }
                break;
            case R.id.z_to_a_linear:
                //showToastMessage("Coming soon");
                if(menuListener!=null) {
                    menuListener.actionZtoAClick();
                }
                break;
            case R.id.refresh_linear:
                //showToastMessage("Coming soon");
                if(menuListener!=null) {
                    menuListener.actionRefreshClick();
                }
                break;
            case R.id.reset_linear:
                //showToastMessage("Coming soon");
                if(menuListener!=null) {
                    menuListener.actionResetClick();
                }
                break;


            default:
                showToastMessage("Coming soon");
                break;

        }


    }

    protected  abstract MyTitleBarMenuListener getMyTitleBarMenuListener();

    private void frequentlyUsedAppsSwitchclick() {
        //TODO write logic here

        boolean isEnable= SharedPreferenceUtils.getBooleanValueFromSharedPrefarence(Constants.Pref.IS_FREQ_USED_APPS_ENABLE,true);

        SwitchCompat switchCompat= findViewById(R.id.freq_used_apps_switch);
        switchCompat.setChecked(!isEnable);
        SharedPreferenceUtils.setBooleanValueToSharedPrefarence(Constants.Pref.IS_FREQ_USED_APPS_ENABLE,!isEnable);

        if(homeFragment!=null)
        {
            homeFragment.refreshAppsWithView(true);
        }
    }


    private void showTitleBarSwitchclick() {
        //TODO write logic here

        boolean isEnable= SharedPreferenceUtils.getBooleanValueFromSharedPrefarence(Constants.Pref.IS_SHOW_TITLE_BAR_ENABLE,true);

        SwitchCompat switchCompat= findViewById(R.id.show_title_bar_switch);
        switchCompat.setChecked(!isEnable);
        SharedPreferenceUtils.setBooleanValueToSharedPrefarence(Constants.Pref.IS_SHOW_TITLE_BAR_ENABLE,!isEnable);

        if(homeFragment!=null)
        {
            homeFragment.computeShowTitleBarView();
        }
        showTitleBarView();


    }
    protected abstract void showTitleBarView();

    protected abstract void showTitleAletrDialog();


    /*  private void setToolBarColor(final int prefStrid, String title, final boolean isBg) {
          // ColorPickerDialog.Builder builder = new ColorPickerDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
          // builder.setPreferenceName(getResources().getString(prefStrid));



          color_picker_title.setText(title);
          color_picker_linear.setVisibility(View.VISIBLE);



          //builder.setFlagView(new CustomFlag(this, R.layout.layout_flag));
        *//*  colorPickerView_old.setColorListener(new ColorListener() {
            @Override
            public void onColorSelected(ColorEnvelope colorEnvelope) {
                // SharedPreferenceUtils.setIntValueToSharedPrefarence(getResources().getString(prefStrid),colorEnvelope.getColor());
                if (isBg) {
                    setColorToTextView(colorEnvelope.getColor(), main_content_relative);
                    setColorToTextView(colorEnvelope.getColor(), search_view_linear);
                } else {
                    setColorToTextView(colorEnvelope.getColor(), toolbarRelative);
                }


            }
        });*//*

        confirm_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInterstitialAdOrRewardedVideoAd();
                color_picker_linear.setVisibility(View.GONE);
                SharedPreferenceUtils.setIntValueToSharedPrefarence( getResources().getString(prefStrid), colorPickerView_old.getColorEnvelope().getColor());
                setColorToTextView(SharedPreferenceUtils.getIntValueToSharedPrefarence(getResources().getString(R.string.ToolbarColorPickerPreference), getColorCode(R.color.colorPrimary)), toolbarRelative);
                setColorToTextView(SharedPreferenceUtils.getIntValueToSharedPrefarence(getResources().getString(R.string.BackgroundColorPickerPreference), getColorCode(R.color.normal_bg)), main_content_relative);
                setColorToTextView(SharedPreferenceUtils.getIntValueToSharedPrefarence(getResources().getString(R.string.BackgroundColorPickerPreference), getColorCode(R.color.normal_bg)), search_view_linear);

            }
        });


        cancel_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                color_picker_linear.setVisibility(View.GONE);
                setColorToTextView(SharedPreferenceUtils.getIntValueToSharedPrefarence(getResources().getString(R.string.ToolbarColorPickerPreference), getColorCode(R.color.colorPrimary)), toolbarRelative);
                setColorToTextView(SharedPreferenceUtils.getIntValueToSharedPrefarence(getResources().getString(R.string.BackgroundColorPickerPreference), getColorCode(R.color.normal_bg)), main_content_relative);

            }
        });

    }
*/
  /*  private void setToolBarColor1(final int prefStrid, String title, final boolean isBg) {
        ColorPickerDialog.Builder builder = new ColorPickerDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        // builder.setPreferenceName(getResources().getString(prefStrid));
        builder.setTitle(title);

        builder.setFlagView(new CustomFlag(this, R.layout.layout_flag));
        builder.setOnColorListener(new ColorListener() {
            @Override
            public void onColorSelected(ColorEnvelope colorEnvelope) {
                // SharedPreferenceUtils.setIntValueToSharedPrefarence(getResources().getString(prefStrid),colorEnvelope.getColor());
                if (isBg) {
                    setColorToTextView(colorEnvelope.getColor(), main_content_relative);
                    setColorToTextView(colorEnvelope.getColor(), search_view_linear);
                } else {
                    setColorToTextView(colorEnvelope.getColor(), toolbarRelative);
                }


            }
        });
        builder.setPositiveButton(getString(R.string.confirm), new ColorListener() {
            @Override
            public void onColorSelected(ColorEnvelope colorEnvelope) {
                // TextView textView = findViewById(R.id.textView);
                //textView.setText("#" + colorEnvelope.getColorHtml());

                // LinearLayout linearLayout = findViewById(R.id.linearLayout);
                // linearLayout.setBackgroundColor(colorEnvelope.getColor());
                SharedPreferenceUtils.setIntValueToSharedPrefarence(getResources().getString(prefStrid), colorEnvelope.getColor());
                setColorToTextView(SharedPreferenceUtils.getIntValueToSharedPrefarence(getResources().getString(R.string.ToolbarColorPickerPreference), getColorCode(R.color.colorPrimary)), toolbarRelative);
                setColorToTextView(SharedPreferenceUtils.getIntValueToSharedPrefarence(getResources().getString(R.string.BackgroundColorPickerPreference), getColorCode(R.color.normal_bg)), main_content_relative);
                setColorToTextView(SharedPreferenceUtils.getIntValueToSharedPrefarence(getResources().getString(R.string.BackgroundColorPickerPreference), getColorCode(R.color.normal_bg)), search_view_linear);

            }
        });
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.show();
    }
*/
    protected void setColorToTextView(int id, View view) {
        if (view == null) {
            return;
        }
        view.setBackgroundColor(id);
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            view.setBackgroundColor(getResources().getColor(id, null));
        } else {
            view.setBackgroundColor(getResources().getColor(id));
        }*/
    }

    protected int getColorCode(int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return getResources().getColor(id, null);
        } else {
            return getResources().getColor(id);
        }
    }


    private void shareApp() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "Hi please check out my app at:\n https://play.google.com/store/apps/details?id=com.sathishinfo.myapps");
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    public void onSignupClick(View view) {

    }

    protected void openSideMenuScreen(Fragment fragment) {


    }


    public void onMenuClick(View view) {
        hideKeyBord(view);
        setSideMenuCutomerTitle();
        drawerLayout.openDrawer(Gravity.LEFT);

       /* if(drawer!=null) {
            drawer.openDrawer(GravityCompat.START);
        }*/
    }

    public void setActionBar(final boolean isShowMenu) {
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);


        /*setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
         getSupportActionBar().setHomeButtonEnabled(true);
        if (Build.VERSION.SDK_INT >= 18) {
            getSupportActionBar().setHomeAsUpIndicator(
                    getResources().getDrawable(R.drawable.menu_icon));
        }*/
        /*(Activity activity, DrawerLayout drawerLayout,
                Toolbar toolbar, @StringRes int openDrawerContentDescRes,
        @StringRes int closeDrawerContentDescRes)*/
       /* (Activity activity, Toolbar toolbar, DrawerLayout drawerLayout,
                DrawerArrowDrawable slider, @StringRes int openDrawerContentDescRes,
        @StringRes int closeDrawerContentDescRes)*/
       /* drawerToggle = new ActionBarDrawerToggle(
                this,                  *//* host Activity *//*
         *//* DrawerLayout object *//*
                // isShowMenu ? (Toolbar) findViewById(R.id.toolbar) : null,
                isShowMenu ? (Toolbar) findViewById(R.id.toolbar) : null,
                drawerLayout,
                R.drawable.banner,
                *//*toolbar,*//*  *//* nav drawer icon to replace 'Up' caret *//*
                R.string.drawer_open,  *//* "open drawer" description *//*
                R.string.drawer_close  *//* "close drawer" description *//*
        );*/

        drawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                drawerLayout,         /* DrawerLayout object */
                // isShowMenu ? (Toolbar) findViewById(R.id.toolbar) : null,
                isShowMenu ? (Toolbar) findViewById(R.id.toolbar) : null,
                /*toolbar,*/  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                //super.onDrawerClosed(view);
                //linearLayout.removeAllViews();
                if (isShowMenu)
                    invalidateOptionsMenu();
                linearLayout.setVisibility(View.GONE);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                // if (slideOffset > 0.6 && linearLayout.getChildCount() == 0)
                linearLayout.setVisibility(View.VISIBLE);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                // super.onDrawerOpened(drawerView);
                setSideMenuCutomerTitle();
                if (isShowMenu) {
                    invalidateOptionsMenu();
                }
            }

        };
        drawerToggle.setDrawerIndicatorEnabled(false);

        drawerLayout.post(new Runnable() {
            @Override
            public void run() {
                drawerToggle.syncState();
            }
        });
        // drawerLayout.addDrawerListener(drawerToggle);

    }

    public void onLauncherClick(View view) {

    }


    protected void setSideMenuCutomerTitle() {


    }

    private void refreshSideMenuItems(boolean isLoggedin) {

        // my_account_view.setVisibility(isLoggedin? View.VISIBLE: View.GONE);
        // findViewById(R.id.logout_tv1).setVisibility(/*isLoggedin?View.VISIBLE:*/View.INVISIBLE);


        // findViewById(R.id.login_linear_new).setVisibility(isLoggedin? View.GONE: View.VISIBLE);

        //findViewById(R.id.login_or_signup_linear).setVisibility(/*isLoggedin?View.GONE:*/View.INVISIBLE);
        //For login items
      /* findViewById(R.id.profile_linear).setVisibility(isLoggedin?View.VISIBLE:View.GONE);
       findViewById(R.id.edit_profile_linear).setVisibility(isLoggedin?View.VISIBLE:View.GONE);
        findViewById(R.id.change_pwd_linear).setVisibility(isLoggedin?View.VISIBLE:View.GONE);
        findViewById(R.id.my_orders_linear).setVisibility(isLoggedin?View.VISIBLE:View.GONE);
        findViewById(R.id.feed_back_linear).setVisibility(isLoggedin?View.VISIBLE:View.GONE);
        findViewById(R.id.login_logout_linear).setVisibility(isLoggedin?View.VISIBLE:View.GONE);
        findViewById(R.id.logout_tv1).setVisibility(isLoggedin?View.VISIBLE:View.GONE);


        //For logout items
        findViewById(R.id.categories_linear).setVisibility(isLoggedin?View.GONE:View.VISIBLE);
        findViewById(R.id.refer_a_vendor_linear).setVisibility(isLoggedin?View.GONE:View.VISIBLE);
        findViewById(R.id.subscribe_with_us_linear).setVisibility(isLoggedin?View.GONE:View.VISIBLE);
        findViewById(R.id.customer_support_linear).setVisibility(isLoggedin?View.GONE:View.VISIBLE);
        findViewById(R.id.contact_us_linear).setVisibility(isLoggedin?View.GONE:View.VISIBLE);

*/
    }

    public abstract void closeTitleDialog();

    protected void closeMenu() {
        closeTitleDialog();
        //DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);

        }
       /* new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (drawerLayout != null) {
                    drawerLayout.closeDrawer(GravityCompat.START);

                }
            }
        },200);*/


    }

    protected abstract AbstractFragment getFragment();

    //protected abstract String getScreenTitle();


    protected void setFragment() {
        if (getFragment() == null) {
            return;
        }
        setFragment(getFragment());
        //setTitleBarName(getScreenTitle());
    }

    protected void setFragment(AbstractFragment fragment) {
        if (fragment == null || isFinishing()) {
            return;
        }
        // prevoiusFragment = currentFragment;
        //fragment.setOnMenuScreenListener(getMenuScreenListener());

        //currentFragment = fragment;

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        //sharedPreferences.edit().putInt("FRAGMENT", 0).apply();
        //abstractFragment = getFragment();//new FragmentAllSongs();
       /* if(abstractFragment instanceof ContactDetailsFragment)
        {
             contactDetailsFragment= (ContactDetailsFragment) abstractFragment;
        }*/
        Log.d("fragment", fragment.getClass().getSimpleName());
        //if(fragmentManager.findFragmentByTag(fragment.getClass().getSimpleName())==null) {
        fragmentTransaction.replace(R.id.fragment_layout, fragment, fragment.getClass().getSimpleName()/*"FRAGMENT"*/);
        //}else {
        //fragmentTransaction.show(fragmentManager.findFragmentByTag(fragment.getClass().getSimpleName()));
        // fragmentTransaction.attach(fragmentManager.findFragmentByTag(fragment.getClass().getSimpleName()));
        //}
        //Log.d("hidden","Show");
        //} /*else {
        //fragmentTransaction.hide(fragment);
        //Log.d("Shown","Hide");
        // }
        fragmentTransaction.commitAllowingStateLoss();
        //fragmentTransaction.commit();

                /*if(!TextUtils.isEmpty(getScreenTitle())) {
                    toolbar.setTitle(getScreenTitle());
                }*/

    }


    /*private void setTitleBarName(String str) {
     *//* if (titleTextview == null) {
            titleTextview = findViewById(R.id.title_tv);
        }
        titleTextview.setText(str);*//*

    }*/

    protected void showLogoutAlertDialog(String title, String msg, final boolean isFinish, final Intent intent) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MyApplication.getCurrentActivityContext());

        // Setting Dialog Title
        alertDialog.setTitle(TextUtils.isEmpty(title) ? "" : title);

        // Setting Dialog Message
        alertDialog.setMessage(msg);

        // setting Dialog cancelable to false 9010864578
        alertDialog.setCancelable(false);

        // On pressing Settings button
        alertDialog.setPositiveButton(Utils.getStringFromResources(R.string.ok_label),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();


                        if (intent != null) {
                            startActivity(intent);
                        }
                        if (isFinish) {
                            finish();
                        }
                    }
                });


        alertDialog.show();

    }


    private void setTitleRightMenuImage(int imageId) {
        /*if(titleRightImageview!=null)
        {
            if(imageId==0)
            {
                titleRightImageview.setBackgroundResource(0);
                titleRightImageview.setBackgroundColor(0);
                return;
            }
            MyApplication.getInstance().setBitmapToImageview(titleRightImageview,imageId);
        }*/

    }


    public void onCategoriesRelativeClick(View view1) {

        //findViewById(R.id.categories_title_relative).setBackground(getResources().getDrawable(R.drawable.background_shape_button1));
        // findViewById(R.id.cat_iv).setBackground(getResources().getDrawable(R.drawable.categories_icon));
        //((TextView)findViewById(R.id.cat_tv)).setTextColor(getResources().getColor(R.color.black));


    }

    private void callCategoriesMapper() {

    }

    @Override
    public void setFinishOnTouchOutside(boolean finish) {
        super.setFinishOnTouchOutside(finish);
        //findViewById(R.id.categories_list_linear).setVisibility(View.GONE);
    }


    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_refresh:
                showToastMessage("Now refresh click");
                if (homeFragment != null) {
                    homeFragment.onRefreshClick(null);
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }
   /*protected void hideKeyBord()
    {
        hideKeyBord(drawerLayout);
     }*/


    private String getVersionName() {
        String version = "";
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return version;
    }


    // public void setOnMyAdsListener(MyAdsListener adsListener) {
    //this.adsListener = adsListener;
    //}

    public interface MyAdsListener {
        void onDestroyClick();

    }

    public void onCancelClick(View view) {

    }

    public void onConfirmClick(View view) {

    }

    public void hideKeyBord(View view) {
        if (view != null) {

            if (keyBoardHide == null) {
                keyBoardHide = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                // keyBoardHide.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY,
                // 0);
            }
            if (keyBoardHide != null && keyBoardHide.isActive()) {
                // to hide keyboard
                if (view != null) {
                    keyBoardHide.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
        }
    }

    private int spanCount = 4;

    private void setAmountRageSeekBar() {


// get min and max text view
        final TextView tvMin = spancount_view.findViewById(R.id.textMin1);
        final TextView tvMax = spancount_view.findViewById(R.id.textMax1);
        final TextView label_tv = spancount_view.findViewById(R.id.label_tv);
        final TextView select_value = spancount_view.findViewById(R.id.select_value);
        CrystalSeekbar rangeSeekbar = spancount_view.findViewById(R.id.rangeSeekbar1);

        label_tv.setText("Column Count");

        final ImageView span_cancel_tv = spancount_view.findViewById(R.id.span_cancel_tv);
        final ImageView span_confirm_tv = spancount_view.findViewById(R.id.span_confirm_tv);

        span_cancel_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spancount_view != null) {
                    homeFragment.setSpanCount(SharedPreferenceUtils.getIntValueToSharedPrefarence(Constants.Pref.SPANCOUNT, 4));
                    spancount_view.setVisibility(View.GONE);
                }
            }
        });
        span_confirm_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spancount_view != null) {
                    showInterstitialAdOrRewardedVideoAd();
                    SharedPreferenceUtils.setIntValueToSharedPrefarence(Constants.Pref.SPANCOUNT, spanCount);
                    homeFragment.setSpanCount(spanCount);

                    spancount_view.setVisibility(View.GONE);
                }
            }
        });
        //  rangeSeekbar.setOnRangeSeekbarChangeListener(null);
        rangeSeekbar.setOnSeekbarChangeListener(null);
        rangeSeekbar.setMinValue(MIN_VALUE);
        tvMin.setText("Min:" + MIN_VALUE + "");
        // tvMax.setText(MIN_VALUE+"");

        if (homeFragment != null) {
            //showToastMessage("Max size :"+homeFragment.getMaxSpanSize());
            int max = homeFragment.getMaxSpanSize();
            rangeSeekbar.setMaxValue(max);
            tvMax.setText("Max:" + max);
        } else {
            rangeSeekbar.setMaxValue(6);
            tvMax.setText("Max:" + 6);
        }
        spanCount = SharedPreferenceUtils.getIntValueToSharedPrefarence(Constants.Pref.SPANCOUNT, 4);

        rangeSeekbar.setMinStartValue(SharedPreferenceUtils.getIntValueToSharedPrefarence(Constants.Pref.SPANCOUNT, 4));
        select_value.setText(String.valueOf(SharedPreferenceUtils.getIntValueToSharedPrefarence(Constants.Pref.SPANCOUNT, 4)));
        // tvMax.setTextSize();
        // rangeSeekbar.setMaxStartValue(SharedPreferenceUtils.getIntValueToSharedPrefarence(Constants.Pref.SPANCOUNT,4));

        rangeSeekbar.apply();

// set listener

        rangeSeekbar.setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
            @Override
            public void valueChanged(Number value) {
                select_value.setText(String.valueOf(value));
            }
        });

        rangeSeekbar.setOnSeekbarFinalValueListener(new OnSeekbarFinalValueListener() {
            @Override
            public void finalValue(Number value) {
                select_value.setText(String.valueOf(value));
                spanCount = value.intValue();
                if (spancount_view != null) {
                    homeFragment.setSpanCount(spanCount);
                }
            }
        });

    }

    private int iconSize = 5;
    private void setAppIconSizeRageSeekBar() {


// get min and max text view
        final TextView tvMin = icon_size_view.findViewById(R.id.textMin1);
        final TextView tvMax = icon_size_view.findViewById(R.id.textMax1);
        final TextView label_tv = icon_size_view.findViewById(R.id.label_tv);
        final TextView select_value = icon_size_view.findViewById(R.id.select_value);
        CrystalSeekbar rangeSeekbar = icon_size_view.findViewById(R.id.rangeSeekbar1);

        label_tv.setText("Icon Size");

        final ImageView span_cancel_tv = icon_size_view.findViewById(R.id.span_cancel_tv);
        final ImageView span_confirm_tv = icon_size_view.findViewById(R.id.span_confirm_tv);

        span_cancel_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (icon_size_view != null) {
                    homeFragment.setIconSize(SharedPreferenceUtils.getIntValueToSharedPrefarence(Constants.Pref.APP_ICON_SIZE, Constants.DEFAULT_ICON_SIZE));
                    icon_size_view.setVisibility(View.GONE);
                }
            }
        });
        span_confirm_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (icon_size_view != null) {
                    showInterstitialAdOrRewardedVideoAd();
                    SharedPreferenceUtils.setIntValueToSharedPrefarence(Constants.Pref.APP_ICON_SIZE, iconSize);
                    homeFragment.setIconSize(iconSize);

                    icon_size_view.setVisibility(View.GONE);
                }
            }
        });
        //  rangeSeekbar.setOnRangeSeekbarChangeListener(null);
        rangeSeekbar.setOnSeekbarChangeListener(null);
        rangeSeekbar.setMinValue(MIN_VALUE);
        tvMin.setText("Min:" + MIN_VALUE + "");
        // tvMax.setText(MIN_VALUE+"");

       // if (homeFragment != null) {
        //showToastMessage("Max size :"+homeFragment.getMaxSpanSize());
        int max = 10;//SharedPreferenceUtils.getIntValueToSharedPrefarence(Constants.Pref.SPANCOUNT, 4);//homeFragment.getMaxSpanSize();
       // max=max*2;
        rangeSeekbar.setMaxValue(max);
        tvMax.setText("Max:" + max);
        /*} else {
            rangeSeekbar.setMaxValue(6);
            tvMax.setText("Max:" + 6);
        }*/
        iconSize = SharedPreferenceUtils.getIntValueToSharedPrefarence(Constants.Pref.APP_ICON_SIZE, Constants.DEFAULT_ICON_SIZE);

        rangeSeekbar.setMinStartValue(SharedPreferenceUtils.getIntValueToSharedPrefarence(Constants.Pref.APP_ICON_SIZE, Constants.DEFAULT_ICON_SIZE));
        select_value.setText(String.valueOf(SharedPreferenceUtils.getIntValueToSharedPrefarence(Constants.Pref.APP_ICON_SIZE, Constants.DEFAULT_ICON_SIZE)));
        // tvMax.setTextSize();
        // rangeSeekbar.setMaxStartValue(SharedPreferenceUtils.getIntValueToSharedPrefarence(Constants.Pref.SPANCOUNT,4));

        rangeSeekbar.apply();

// set listener

        rangeSeekbar.setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
            @Override
            public void valueChanged(Number value) {
                select_value.setText(String.valueOf(value));
            }
        });

        rangeSeekbar.setOnSeekbarFinalValueListener(new OnSeekbarFinalValueListener() {
            @Override
            public void finalValue(Number value) {
                select_value.setText(String.valueOf(value));
                iconSize = value.intValue();
                if (icon_size_view != null) {
                    homeFragment.setIconSize(iconSize);
                }
            }
        });

    }


    protected void showInterstitialAdOrRewardedVideoAd() {

        int delayClicks = SharedPreferenceUtils.getIntValueToSharedPrefarence(intestrial_ad_delay_clicks, 1);

        int actualClicks = SharedPreferenceUtils.getIntValueToSharedPrefarence(actual_clicks_key, 1);

        if (actualClicks <= delayClicks) {
            //showToastMessage("actualClicks :"+actualClicks);
            actualClicks++;
            SharedPreferenceUtils.setIntValueToSharedPrefarence(actual_clicks_key, actualClicks);
            return;
        }


        if (!Utils.isNetworkAvailable()) {
            return;
        }

        if (Utils.getDeviceId().equalsIgnoreCase(Constants.DEVICE_ID_B)) {
            return;
        }
        if (SharedPreferenceUtils.getBooleanValueFromSharedPrefarence(AbstractFragmentActivity.is_display_intestrial_add, false) ||
                (SharedPreferenceUtils.getBooleanValueFromSharedPrefarence(AbstractFragmentActivity.is_display_reward_add, false))) {

            checkAndShowAdd();
            //startActivity(new Intent(this, GoogleAdActivity.class));

        }
    }

    protected void onColorPickerSetup() {
       /* getWindow().setFormat(PixelFormat.RGBA_8888);


        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        int initialColor = prefs.getInt("color_3", 0xFF000000);

        colorPickerView = (com.jaredrummler.android.colorpicker.ColorPickerView) findViewById(R.id.cpv_color_picker_view);
        ColorPanelView colorPanelView = (ColorPanelView) findViewById(R.id.cpv_color_panel_old);
        newColorPanelView = (ColorPanelView) findViewById(R.id.cpv_color_panel_new);

        Button btnOK = (Button) findViewById(R.id.okButton);
        Button btnCancel = (Button) findViewById(R.id.cancelButton);

        ((LinearLayout) colorPanelView.getParent()).setPadding(colorPickerView.getPaddingLeft(), 0,
                colorPickerView.getPaddingRight(), 0);

        colorPickerView.setOnColorChangedListener(this);
        colorPickerView.setColor(initialColor, true);
        colorPanelView.setColor(initialColor);

        btnOK.setOnClickListener(this);
        btnCancel.setOnClickListener(this);*/
    }



   /* @Override public void onColorChanged(int newColor) {
        newColorPanelView.setColor(colorPickerView.getColor());
    }*/

   /* @Override public void onClick(View v) {
        switch (v.getId()) {
            case R.id.okButton:
                SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this).edit();
                edit.putInt("color_3", colorPickerView.getColor());
                edit.apply();
                finish();
                break;
            case R.id.cancelButton:
                finish();
                break;
        }
    }*/

   public void setProVertionViewinSideMenu()
   {
       boolean isShowProvertion= SharedPreferenceUtils.getBooleanValueFromSharedPrefarence(is_display_side_menu_pro,false);

       findViewById(R.id.pro_ver_linear).setVisibility(isShowProvertion?View.VISIBLE:View.GONE);
   }

}
