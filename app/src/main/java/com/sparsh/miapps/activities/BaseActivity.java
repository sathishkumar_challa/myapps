package com.sparsh.miapps.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Window;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.sparsh.miapps.R;
import com.sparsh.miapps.application.MyApplication;
import com.sparsh.miapps.utils.Constants;
import com.sparsh.miapps.utils.SharedPreferenceUtils;
import com.sparsh.miapps.utils.Utils;

//import com.google.firebase.database.Transaction;

public abstract class BaseActivity extends SwipedRecyclerViewActivity {
    private static final String TAG ="BaseActivity" ;
    FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        initValues();

        try {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        } catch (Exception e) {

        }

        /*FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener(){
            @Override
            public void onSuccess(Object instanceIdResult) {

                Transaction.Result obj= (Transaction.Result) instanceIdResult;
               // TResult asd;
                Log.i("Device token", obj.getToken());
            }
        });*/

       /* FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(instanceIdResult -> {
            Log.i("Device token", instanceIdResult.getToken());
        });
*/
/*
		Bundle bundle = new Bundle();
		bundle.putString(FirebaseAnalytics.Param.ITEM_ID, id);
		bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, name);
		bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
		mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)
*/

		/*final Fabric fabric = new Fabric.Builder(this)
				.kits(new Crashlytics())
				.debuggable(true)  // Enables Crashlytics debugger
				.build();
		Fabric.with(fabric);*/
        FirebaseApp.initializeApp(this);


        requestWindowFeature(Window.FEATURE_NO_TITLE);
        MyApplication.setCurrentActivityContext(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.setCurrentActivityContext(this);
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(this, getPackageName(), null);

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(instanceIdResult -> {
            Log.i(TAG,"Device Token :"+instanceIdResult.getToken());
        });
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        MyApplication.setCurrentActivityContext(this);
    }


    public void showAlertDialog(String title, int msgId, final boolean isFinish) {
        showAlertDialog(title, Utils.getStringFromResources(msgId, this), isFinish, null);
    }

    public void showAlertDialog(String title, String msg, final boolean isFinish) {
        showAlertDialog(title, msg, isFinish, null);
    }

    protected void showAlertDialog(String title, String msg, final boolean isFinish, final Intent intent) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle(TextUtils.isEmpty(title) ? "" : title);

        // Setting Dialog Message
        alertDialog.setMessage(msg);

        // setting Dialog cancelable to false 9010864578
        alertDialog.setCancelable(false);

        // On pressing Settings button
        alertDialog.setPositiveButton(Utils.getStringFromResources(R.string.ok_label, this),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();


                        if (intent != null) {
                            startActivity(intent);
                        }
                        if (isFinish) {
                            finish();
                        }
                    }
                });


        alertDialog.show();

    }

    protected void rateIt() {
        startActivity(new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://play.google.com/store/apps/details?id=com.sathishinfo.myapps")));
    }

    protected void termsAndConditions() {
        startActivity(new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://www.sathishinfo.in/myapps/terms_and_conditions.html")));
    }


    private void initValues() {
        boolean isInit = SharedPreferenceUtils.getBooleanValueFromSharedPrefarence(Constants.Pref.IS_INIT_VALUES, false);

        if (!isInit) {
            SharedPreferenceUtils.setBooleanValueToSharedPrefarence(Constants.Pref.IS_FREQ_USED_APPS_ENABLE, true);

            SharedPreferenceUtils.setBooleanValueToSharedPrefarence(Constants.Pref.IS_INIT_VALUES, true);
            SharedPreferenceUtils.setBooleanValueToSharedPrefarence(Constants.Pref.IS_SHOW_TITLE_BAR_ENABLE, false);

        }

    }

}
