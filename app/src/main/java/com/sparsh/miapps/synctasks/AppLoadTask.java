package com.sparsh.miapps.synctasks;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.sparsh.miapps.core.App;
import com.sparsh.miapps.database.ApplicationsDatabase;
import com.sparsh.miapps.utils.Constants;
import com.sparsh.miapps.utils.Utils;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;


/**
 * Inner static class for loading the Installed Applications Asynchronously
 * 
 * @author android
 * 
 */
public class AppLoadTask extends AsyncTask<Void, Void, ArrayList<App>> {

	private static final String TAG = "AppLoadTask";
	private Activity activity;
	private boolean isFirstTime;
	private ProgressDialog dialog;
	private ApplicationsDatabase db;
	boolean isRefresh;
	private InputMethodManager keyBoardHide;

	private AppsLoadListener appsLoadListener;

	public AppLoadTask(Activity activity,boolean isRefresh) {
		this.activity = activity;
		this.isRefresh=isRefresh;

	}

	public void setAppsLoadListener(AppsLoadListener appsLoadListener)
	{
		this.appsLoadListener=appsLoadListener;
	}

	/*
	 * Creating a progress dialog for showing status (non-Javadoc)
	 * 
	 * @see android.os.AsyncTask#onPreExecute()
	 */
	@SuppressLint("InlinedApi")
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
			dialog = new ProgressDialog(activity,
					AlertDialog.THEME_DEVICE_DEFAULT_DARK);
		} else {
			dialog = new ProgressDialog(activity);
		}
		dialog.setMessage("Please wait...");
		dialog.setCancelable(false);
		dialog.show();
		db = new ApplicationsDatabase(activity);
		hideKeyBord(dialog.getCurrentFocus());

	}

	@Override
	protected ArrayList<App> doInBackground(Void... params) {
		try {
			PackageManager packageManager = activity.getPackageManager();
			if(isRefresh)
			{
				isFirstTime = true;
				db.removeAllApplications();
				return getInstalledApps(packageManager);
			}


			ArrayList<App> list = db.getApplications();
			if (list != null && list.size() != 0) {
				return list;
			} else {
				isFirstTime = true;
				return getInstalledApps(packageManager);
			}
		} catch (Exception e) {
		}
		return null;
	}

	/**
	 * Used to get all the installed applications
	 * 
	 * @param packageManager
	 * @return list of installed apps
	 */
	private ArrayList<App> getInstalledApps(PackageManager packageManager) {
		try {

			ArrayList<App> list = new ArrayList<App>();
			final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
			mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
			List<ResolveInfo> intentActivities = packageManager
					.queryIntentActivities(mainIntent, 0);
			//List<ApplicationInfo> installedApplications = packageManager.getInstalledApplications(0);


			for (ResolveInfo resolveInfo : intentActivities) {
				App app = new App(false);
				app.appName = (String) resolveInfo.loadLabel(packageManager);
				app.packageName = resolveInfo.activityInfo.packageName;

				boolean userApp = isUserApp(resolveInfo.activityInfo.applicationInfo);
				app.isUserApp=userApp?1:0;
				//if (isUserApp(resolveInfo.activityInfo.applicationInfo)) {
					list.add(app);
				/*}else{
					db.addSystemApplication(app);
					
				}*/
			}
			return list;
		} catch (Exception e) {
			Utils.showDebugLog(TAG, e, activity);
		}
		return null;
	}

	boolean isUserApp(ApplicationInfo ai) {
		int mask = ApplicationInfo.FLAG_SYSTEM
				| ApplicationInfo.FLAG_UPDATED_SYSTEM_APP;
		return (ai.flags & mask) == 0;
	}

	@Override
	protected void onPostExecute(ArrayList<App> result) {
		super.onPostExecute(result);

		try {
			if (isFirstTime) {
				for (App app : result) {
					db.addApplication(app);
				}
				if(appsLoadListener!=null) {
					appsLoadListener.loadComplete();
				}
			}
			if (dialog != null && dialog.isShowing()) {
				dialog.dismiss();
			}

			LocalBroadcastManager.getInstance(activity).sendBroadcast(
					new Intent(Constants.MY_INTENT_FILTER));


		} catch (Exception e) {
			Utils.showDebugLog(TAG, e, activity);
		}
	}

	public void hideKeyBord(View view) {
		if (view != null) {

			if (keyBoardHide == null) {
				keyBoardHide = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
				// keyBoardHide.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY,
				// 0);
			}
			if (keyBoardHide != null && keyBoardHide.isActive()) {
				// to hide keyboard
				if (view != null) {
					keyBoardHide.hideSoftInputFromWindow(view.getWindowToken(), 0);
				}
			}
		}
	}

	public interface AppsLoadListener
	{
		void loadComplete();
	}
}
