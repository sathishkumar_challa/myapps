package com.sparsh.miapps.colorpicker;

import android.os.Bundle;
import android.view.MenuItem;

import com.jaredrummler.android.colorpicker.ColorPreferenceCompat;
import com.sparsh.miapps.R;
import com.sparsh.miapps.utils.SharedPreferenceUtils;

import androidx.preference.Preference;

public class DemoFragment extends BasePreferenceFragment implements Preference.OnPreferenceChangeListener {

    private static final String TAG = "DemoFragment";

    // public static final String APP_BG_COLOR = "app_bg_color";
    private static final String APP_NAME_COLOR = "app_name_color";
    // private static final String TOOLBAR_BG_COLOR = "toolbar_bg_color";
    private static final String TITLE_COLOR = "title_color";
   /* @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.layout.preferences);
    }*/


    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.main, rootKey);

        // Example showing how we can get the new color when it is changed:
   /* ColorPreferenceCompat bgcolorPreference = (ColorPreferenceCompat) findPreference(getResources().getString(R.string.BackgroundColorPickerPreference));
      if(bgcolorPreference!=null) {
          bgcolorPreference.setOnPreferenceChangeListener(this);
      }*/
        setPrefListener(R.string.BackgroundColorPickerPreference);
        setPrefListener(R.string.ToolbarColorPickerPreference);

        setPrefListener(R.string.app_name_color);
        setPrefListener(R.string.title_color);
        setPrefListener(R.string.sub_title_color);

        // done_btn

        Preference doneBtnPreference = (Preference) findPreference("done_btn");

        if (doneBtnPreference != null) {
            doneBtnPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                   // getActivity().finish();
                    getActivity().onBackPressed();

                    return true;
                }
            });
        }

        // ColorPreferenceCompat toolbarcolorPreference = (ColorPreferenceCompat) findPreference(getResources().getString(R.string.ToolbarColorPickerPreference));
        //toolbarcolorPreference.setOnPreferenceChangeListener(this);


    /*new Preference.OnPreferenceChangeListener() {
      @Override public boolean onPreferenceChange(Preference preference, Object newValue) {
        if (getResources().getString(R.string.BackgroundColorPickerPreference).equals(preference.getKey())) {
         // String newDefaultColor = Integer.toHexString((int) newValue);
         // Log.d(TAG, "New default color is: #" + newDefaultColor);
          SharedPreferenceUtils.setIntValueToSharedPrefarence( getResources().getString(R.string.BackgroundColorPickerPreference), (int) newValue);

        }
        return true;
      }
    });*/
    }

    private void setPrefListener(int id) {
        ColorPreferenceCompat bgcolorPreference = (ColorPreferenceCompat) findPreference(getResources().getString(id));
        if (bgcolorPreference != null) {
            bgcolorPreference.setOnPreferenceChangeListener(this);
        }
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {

        SharedPreferenceUtils.setIntValueToSharedPrefarence(preference.getKey(), (int) newValue);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            getActivity().onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
