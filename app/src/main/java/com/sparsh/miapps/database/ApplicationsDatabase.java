package com.sparsh.miapps.database;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.sparsh.miapps.constants.DatabaseConstatnts;
import com.sparsh.miapps.core.App;


public class ApplicationsDatabase extends SQLiteOpenHelper {
	private static final String TAG = "ApplicationsDatabase";
	private String APPS_TABLE_NAME = "APPLICATIONS";
	private String SYS_APPS_TABLE_NAME = "SYSTEM_APPLICATIONS";
	private String PACKAGENAME = "PACKAGENAME";
	private String APP_COUNT = "APP_COUNT";
	private String APPLICATIONNAME = "APPLICATIONNAME";
	private String IS_USER_APP = "is_user_app";

	public ApplicationsDatabase(Context context) {
		super(context, DatabaseConstatnts.DATABASE_NAME, null,
				DatabaseConstatnts.VERSION);
	}

	/*
	 * Called when the first time of db creating
	 * 
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite
	 * .SQLiteDatabase)
	 */
	@Override
	public void onCreate(SQLiteDatabase arg0) {
		createTable(arg0);
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int arg1, int arg2) {
		//DROP TABLE table_name;
		//database.delete(APPS_TABLE_NAME);
		database.execSQL("DROP TABLE "+APPS_TABLE_NAME+";");
		createTable(database);

		/*database.execSQL("CREATE TABLE " + APPS_TABLE_NAME + " (" + "ID"
				+ " integer PRIMARY KEY AUTOINCREMENT , "
				+ PACKAGENAME + " varchar ,"
				+ APP_COUNT + " integer ,"
				+ APPLICATIONNAME + " varchar)");*/
	}



	/**
	 * Executing create table command
	 * 
	 * @param database
	 */
	private void createTable(SQLiteDatabase database) {
		database.execSQL("CREATE TABLE " + APPS_TABLE_NAME + " (" + "ID"
				+ " integer PRIMARY KEY AUTOINCREMENT , "
				+ PACKAGENAME + " varchar ,"
				+ APP_COUNT + " integer ,"
				+ IS_USER_APP + " integer ,"
				+ APPLICATIONNAME + " varchar)");

		/*database.execSQL("CREATE TABLE " + APPS_TABLE_NAME + " (" + " ID "
				+ " integer auto increment , " + PACKAGENAME + " varchar ,"
				+ APPLICATIONNAME + " varchar)");
*/
	/*	database.execSQL("CREATE TABLE " + SYS_APPS_TABLE_NAME + " (" + "ID"
				+ " integer auto increment , " + PACKAGENAME + " varchar ,"
				+ APPLICATIONNAME + " varchar)");*/
	}



	public void addApplication(App application) {
		try {
			if (!isApplicationExists(application)) {
				ContentValues insert = new ContentValues();
				insert.put(PACKAGENAME, application.packageName);
				insert.put(APPLICATIONNAME, application.appName);
				insert.put(IS_USER_APP,application.isUserApp);
				// insert.put(PROTECTED, application.isChecked);
				SQLiteDatabase database = getWritableDatabase();
				database.insert(APPS_TABLE_NAME, null, insert);
				database.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Remove the given application from the database
	 * 
	 * @param application
	 * @return
	 */
	public void removeApplication(App application) {
		try {
			SQLiteDatabase database = getWritableDatabase();
			// if (isApplicationExists(application.packageName)) {
			database.delete(APPS_TABLE_NAME, PACKAGENAME + "=?",
					new String[] { application.packageName });
			// }
			database.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void removeAllApplications() {
		try {
			SQLiteDatabase database = getWritableDatabase();
			// if (isApplicationExists(application.packageName)) {
			database.delete(APPS_TABLE_NAME, null,null);
			// }
			database.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void increaseAppCount(App app) {
		try {
			SQLiteDatabase database = getWritableDatabase();
			// if (isApplicationExists(application.packageName)) {
			/*database.delete(APPS_TABLE_NAME, null,null);
			ContentValues values= new ContentValues();
			values.put(APP_COUNT,app.count);
			int update=database.update(APPS_TABLE_NAME,values,"ID="+app.id,
					null);*/

		  String qry="UPDATE "+APPS_TABLE_NAME+" SET "+APP_COUNT+" = "+app.count+" WHERE ID = "+app.id+";";
		Cursor cursor=	 database.rawQuery(qry,null);

			if (cursor != null & cursor.moveToFirst()) {
				cursor.close();
				Log.i(TAG,"Updated success "+cursor.getCount());
			}
			//cursor.close();
			database.close();
			// }
			database.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateAppCount(App app) {
		try {
			SQLiteDatabase database = getWritableDatabase();
			// if (isApplicationExists(application.packageName)) {
			/*database.delete(APPS_TABLE_NAME, null,null);
			ContentValues values= new ContentValues();
			values.put(APP_COUNT,app.count);
			int update=database.update(APPS_TABLE_NAME,values,"ID="+app.id,
					null);*/

			String qry="UPDATE "+APPS_TABLE_NAME+" SET "+APP_COUNT+" = "+app.count+" WHERE "+PACKAGENAME+" = \'"+app.packageName+"\';";
			Cursor cursor=	 database.rawQuery(qry,null);

			if (cursor != null & cursor.moveToFirst()) {
				cursor.close();
				Log.i(TAG,"Updated success "+cursor.getCount());
			}
			//cursor.close();
			database.close();
			// }
			database.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int getCount() {
		SQLiteDatabase database = getWritableDatabase();
		Cursor cursor = database.query(APPS_TABLE_NAME, new String[] { "ID" },
				null, null, null, null, null);
		if (cursor != null) {
			int count = cursor.getCount();
			cursor.close();
			return count;
		}
		return 0;
	}

	/**
	 * All the applications list
	 * 
	 * @return list of locked applications
	 */
	public ArrayList<App> getApplications() {
		SQLiteDatabase database = getWritableDatabase();
		Cursor cursor = database.query(APPS_TABLE_NAME, null, null, null, null,
				null, PACKAGENAME);
		ArrayList<App> applications = new ArrayList<App>();
		try {
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {
						App application = new App(false);
						application.packageName = cursor.getString(cursor
								.getColumnIndex(PACKAGENAME));
						application.id=cursor.getInt(cursor
								.getColumnIndex("ID" ));
						application.count=cursor.getInt(cursor
								.getColumnIndex(APP_COUNT ));
						application.appName = cursor.getString(cursor
								.getColumnIndex(APPLICATIONNAME));
						application.isUserApp=cursor.getInt(cursor
								.getColumnIndex(IS_USER_APP));
						applications.add(application);
					} while (cursor.moveToNext());
				}
			}
			cursor.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		database.close();
		return applications;
	}

	public ArrayList<App> getAllRecentApplications() {
		SQLiteDatabase database = getWritableDatabase();
		/*Cursor cursor1 = database.query(APPS_TABLE_NAME, null,
				null, null, null,
				null, "ORDER BY "+APP_COUNT+" ASC");*/

		//limit=limit*2;
		//int limit = 15;
		/*if(limit<8)
		{
			limit = 8;
		}*/

		Cursor cursor = database.rawQuery("SELECT * FROM "+APPS_TABLE_NAME+" WHERE "+APP_COUNT+"<> 0 ORDER BY "+APP_COUNT,null);
		ArrayList<App> applications = new ArrayList<App>();
		try {
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					int count=cursor.getCount();
					/*if(count<3) {
						return applications;
					}*/
					do {
						App application = new App(false);
						application.packageName = cursor.getString(cursor
								.getColumnIndex(PACKAGENAME));
						application.id=cursor.getInt(cursor
								.getColumnIndex("ID" ));
						application.count=cursor.getInt(cursor
								.getColumnIndex(APP_COUNT ));
						application.appName = cursor.getString(cursor
								.getColumnIndex(APPLICATIONNAME));
						application.isUserApp=cursor.getInt(cursor
								.getColumnIndex(IS_USER_APP));
						applications.add(application);
					} while (cursor.moveToNext());
				}
			}
			cursor.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		database.close();

		return applications;
	}

	public ArrayList<App> getAllNonRecentApplications() {
		SQLiteDatabase database = getWritableDatabase();
		/*Cursor cursor1 = database.query(APPS_TABLE_NAME, null,
				null, null, null,
				null, "ORDER BY "+APP_COUNT+" ASC");*/

		//limit=limit*2;
		//int limit = 15;
		/*if(limit<8)
		{
			limit = 8;
		}*/

		Cursor cursor = database.rawQuery("SELECT * FROM "+APPS_TABLE_NAME+" WHERE "+APP_COUNT+"= '0' OR  "+APP_COUNT+"= 0 || "+APP_COUNT+"= ''",null);
		ArrayList<App> applications = new ArrayList<App>();
		try {
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					int count=cursor.getCount();
					/*if(count<3) {
						return applications;
					}*/
					do {
						App application = new App(false);
						application.packageName = cursor.getString(cursor
								.getColumnIndex(PACKAGENAME));
						application.id=cursor.getInt(cursor
								.getColumnIndex("ID" ));
						application.count=cursor.getInt(cursor
								.getColumnIndex(APP_COUNT ));
						application.appName = cursor.getString(cursor
								.getColumnIndex(APPLICATIONNAME));
						application.isUserApp=cursor.getInt(cursor
								.getColumnIndex(IS_USER_APP));
						applications.add(application);
					} while (cursor.moveToNext());
				}
			}
			cursor.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		database.close();

		return applications;
	}


	public ArrayList<App> getRecentApplications() {
		SQLiteDatabase database = getWritableDatabase();
		/*Cursor cursor1 = database.query(APPS_TABLE_NAME, null,
				null, null, null,
				null, "ORDER BY "+APP_COUNT+" ASC");*/

		//limit=limit*2;
		int limit = 15;
		/*if(limit<8)
		{
			limit = 8;
		}*/

		Cursor cursor = database.rawQuery("SELECT * FROM "+APPS_TABLE_NAME+" WHERE "+APP_COUNT+"<> 0 ORDER BY "+APP_COUNT+" DESC LIMIT "+limit,null);
		ArrayList<App> applications = new ArrayList<App>();
		try {
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					int count=cursor.getCount();
					/*if(count<3) {
						return applications;
					}*/
					do {
						App application = new App(false);
						application.packageName = cursor.getString(cursor
								.getColumnIndex(PACKAGENAME));
						application.id=cursor.getInt(cursor
								.getColumnIndex("ID" ));
						application.count=cursor.getInt(cursor
								.getColumnIndex(APP_COUNT ));
						application.appName = cursor.getString(cursor
								.getColumnIndex(APPLICATIONNAME));
						application.isUserApp=cursor.getInt(cursor
								.getColumnIndex(IS_USER_APP));
						applications.add(application);
					} while (cursor.moveToNext());
				}
			}
			cursor.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		database.close();
		return applications;
	}

	/**
	 * @return True given application is exist
	 */
	private boolean isApplicationExists(App app) {
		try {
			SQLiteDatabase database = getWritableDatabase();
			Cursor cursor = database.query(APPS_TABLE_NAME, null,
					 "ID=? ", new String[] { app.id+"" }, null, null, null);
			if (cursor != null & cursor.moveToFirst()) {
				cursor.close();
				return true;
			}
			cursor.close();
			database.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public void addSystemApplication(App application) {
		try {
			ContentValues insert = new ContentValues();
			insert.put(PACKAGENAME, application.packageName);
			insert.put(APPLICATIONNAME, application.appName);
			// insert.put(PROTECTED, application.isChecked);
			SQLiteDatabase database = getWritableDatabase();
			database.insert(SYS_APPS_TABLE_NAME, null, insert);
			database.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public boolean isSystemApplication(String packageName) {
		try {
			SQLiteDatabase database = getWritableDatabase();
			Cursor cursor = database.query(SYS_APPS_TABLE_NAME, null,
					PACKAGENAME + "=? ", new String[] { packageName }, null,
					null, null);
			if (cursor != null & cursor.moveToFirst()) {
				cursor.close();
				return true;
			}
			cursor.close();
			database.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}
