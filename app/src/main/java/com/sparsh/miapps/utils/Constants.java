package com.sparsh.miapps.utils;

public interface Constants {

    int DEFAULT_ICON_SIZE=5;


    String MY_INTENT_FILTER = "com.sathishinfo.myapps.updatelist";
    boolean IS_TESTING=DeviceInfo.isMyDevice();

   String DEVICE_ID_B= "1008e75affe349a7";
    interface Pref {

        String SPANCOUNT = "spancount";
        String APP_TITLE_KEY = "app_title_key";
        String IS_FREQ_USED_APPS_ENABLE = "is_freq_used_apps_enable";
        String IS_INIT_VALUES = "is_init_values1";
        String FREQ_USED_PACKAGE_NAME = "freq_used_package_name";
        String APP_ICON_SIZE = "app_icon_size";

        String IS_SHOW_TITLE_BAR_ENABLE="is_show_title_bar_enabled";
        //String IS_DISPLAY_UPDATE_APP_DIALOG = "update_app_dialog";
    }

    interface AppsViewType{
        String APPS_VIEW_TYPE_KEY="apps_view_type_key";
        int A_TO_Z=1;
        int Z_TO_A=2;
        //int MOST_USED=3;

    }


    class DeviceInfo
    {
        public static boolean isMyDevice()
        {
            if(Utils.getDeviceId().equalsIgnoreCase("8ae13557eb6f13e9"))
            {
                return true;
            }else
            {
                return false;
            }
        }
    }


    /*interface RangeBarValues {
        float MIN_VALUE = 100;
        float MAX_VALUE = 10000;
    }*/
}
