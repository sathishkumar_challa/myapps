package com.sparsh.miapps.interfaces;

/**
 * Created by Sathish Kumar Challa on 24/11/18.
 */
public interface HomeActivityListener {
    void showToolBar();
}
