package com.sparsh.miapps.fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.inputmethod.InputMethodManager;
import android.widget.ScrollView;

import com.sparsh.miapps.R;
import com.sparsh.miapps.application.MyApplication;
import com.sparsh.miapps.utils.Utils;

import androidx.fragment.app.Fragment;

/**
 * Created by satish on 17/11/17.
 */

public abstract class AbstractFragment extends Fragment {


    private InputMethodManager keyBoardHide;
    private ProgressDialog progress;






    public  void onTitleRightButtonClick(){

    }

    public void setScreenTitle(String screenTitle) {
        if(TextUtils.isEmpty(screenTitle) || getFragemtView()==null)
        {
            return;
        }
        // if(screen_title_tv==null) {
        //TextView    screen_title_tv = getFragemtView().findViewById(R.id.screen_title_tv);
        // }
        /*if(screen_title_tv==null) {
            return;
        }
        screen_title_tv.setText(screenTitle);*/
        // screen_title_tv get
    }


     View getFragemtView(){
        return null;
    }




    public void showAlertDialog(String title, int msgId,final boolean isFinish) {
        showAlertDialog(title, Utils.getStringFromResources(msgId),isFinish, null);
    }
    public void showAlertDialog(String title, String msg,final boolean isFinish) {
        showAlertDialog(title, msg,isFinish, null);
    }

    protected void showAlertDialog(String title, String msg,final boolean isFinish, final Intent intent) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MyApplication.getCurrentActivityContext());

        // Setting Dialog Title
        alertDialog.setTitle(TextUtils.isEmpty(title)?"":title);

        // Setting Dialog Message
        alertDialog.setMessage(msg);

        // setting Dialog cancelable to false 9010864578
        alertDialog.setCancelable(false);

        // On pressing Settings button
        alertDialog.setPositiveButton(Utils.getStringFromResources(R.string.ok_label),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();


                        if(intent!=null)
                        {
                            startActivity(intent);
                        }
                        if(isFinish)
                        {
                            ((Activity) MyApplication.getCurrentActivityContext()).finish();
                        }
                    }
                });


        alertDialog.show();

    }


    protected void showProgressView()
    {
        hideProgressView();
        progress = new ProgressDialog(MyApplication.getCurrentActivityContext());
        if(progress==null)
        {
            return;
        }
        progress.setMessage("Loading...");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.show();
    }

    protected void hideProgressView()
    {
        if(progress!=null)
        {
            progress.dismiss();
            progress=null;
        }
    }










    public void hideKeyBord(View view) {
        if (view != null) {
            if (keyBoardHide == null) {
                keyBoardHide = (InputMethodManager) MyApplication.getCurrentActivityContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                // keyBoardHide.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY,
                // 0);
            }
            if (keyBoardHide != null && keyBoardHide.isActive()) {
                // to hide keyboard
                if (view != null) {
                    keyBoardHide.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
        }
    }


    /**
     * Used to scroll to the given view.
     *
     * @param scrollViewParent Parent ScrollView
     * @param view             View to which we need to scroll.
     */
    protected void scrollToView(final ScrollView scrollViewParent, final View view) {
        // Get deepChild Offset
        Point childOffset = new Point();
        getDeepChildOffset(scrollViewParent, view.getParent(), view, childOffset);
        // Scroll to child.
        scrollViewParent.smoothScrollTo(0, childOffset.y);
    }

    /**
     * Used to get deep child offset.
     * <p/>
     * 1. We need to scroll to child in scrollview, but the child may not the direct child to scrollview.
     * 2. So to get correct child position to scroll, we need to iterate through all of its parent views till the main parent.
     *
     * @param mainParent        Main Top parent.
     * @param parent            Parent.
     * @param child             Child.
     * @param accumulatedOffset Accumalated Offset.
     */
    private void getDeepChildOffset(final ViewGroup mainParent, final ViewParent parent, final View child, final Point accumulatedOffset) {
        ViewGroup parentGroup = (ViewGroup) parent;
        accumulatedOffset.x += child.getLeft();
        accumulatedOffset.y += child.getTop();
        if (parentGroup.equals(mainParent)) {
            return;
        }
        getDeepChildOffset(mainParent, parentGroup.getParent(), parentGroup, accumulatedOffset);
    }









    public interface DownloadPDFAsURIListener
    {
        void onComplete(Uri uri);
    }


    protected void showPDFInScreen(Uri uri)
    {

    }


  /*  protected void showAlertDialog(String title, String msg, final MyDialogInterface dialogInterface) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MyApplication.getCurrentActivityContext());

        // Setting Dialog Title
        alertDialog.setTitle((TextUtils.isEmpty(title))? "" : title);

        // Setting Dialog Message
        alertDialog.setMessage(msg);

        // setting Dialog cancelable to false 9010864578
        alertDialog.setCancelable(false);

        // On pressing Settings button
        alertDialog.setPositiveButton(Utils.getStringFromResources(R.string.ok_label), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               if(dialogInterface!=null)
               {
                   dialogInterface.onPossitiveClick();
               }
            }
        });
        alertDialog.setNegativeButton(Utils.getStringFromResources(R.string.cancel_lbl), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(dialogInterface!=null)
                {
                    dialogInterface.onNegetiveClick();
                }
            }
        });




        alertDialog.show();

    }



    protected boolean isValidResponse(AbstractResponse response, String errorMessage*//*,boolean isShowDialog,boolean isShowNothing*//*) {

        return isValidResponse(response, errorMessage, false, false);
    }

    protected boolean isValidResponse(AbstractResponse response, String errorMessage, boolean isShowDialog, boolean isFinish) {
        if (response == null && TextUtils.isEmpty(errorMessage)) {
            if (isShowDialog) {
                showAlertDialog("Alert", Utils.getStringFromResources(R.string.invalid_service_response_lbl), isFinish);
            } else {
                Utils.showToastMsg(R.string.invalid_service_response_lbl);
            }
            return false;
        }
        if (response == null && !TextUtils.isEmpty(errorMessage)) {
            if (isShowDialog) {
                showAlertDialog("Alert", errorMessage, isFinish);
            } else {
                Utils.showToastMsg(errorMessage);
            }
            return false;
        }
        if (!response.getStatusCode().equalsIgnoreCase(Constants.SUCCESS_STATUS_CODE)) {
            if (isShowDialog) {
                showAlertDialog("Alert", response.getStatusMessage(), isFinish);
            } else {
                Utils.showToastMsg(response.getStatusMessage());
            }
            //showMyAlertDialog("Alert",response.getStatusMessage() ,"Ok",false);

            return false;

        }
        return true;
    }

*/
}
