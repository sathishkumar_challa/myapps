package com.sparsh.miapps.fragments;

import android.view.View;

import com.sparsh.miapps.R;
import com.sparsh.miapps.views.FixedRecyclerView;
import com.sparsh.miapps.views.IndexFastScrollRecyclerView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


/**
 * Created by sathishkumar on 9/6/16.
 */
abstract class SwipedRecyclerViewFragment extends AbstractFragment {
    protected SwipeRefreshLayout swipeLayout;
    protected IndexFastScrollRecyclerView recyclerView;
    protected boolean loading = false;
    protected boolean isFirstLoading;



    protected void setupSwipeRefreshLayout(View view) {
        swipeLayout = null;//view.findViewById(R.id.swipe_container);
        if (swipeLayout == null) {
            return;
        }
        // swipeLayout.setEnabled(false);
        // swipeLayout.setVisibility(View.VISIBLE);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {

                //swipeLayout.setRefreshing(false);

                onSwipeLayoutSetToDefaultPage();
                return;


            }

        });
        swipeLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    /*protected void showNoRecordFoundTextView(boolean isShow) {
        if (noRecordsFoundRelative == null) {
            return;
        }
        noRecordsFoundRelative.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }*/

    protected void addListenersToRecyclerView() {
        if (recyclerView == null) {
            return;
        }
        isFirstLoading=true;

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

       /* recyclerView.addOnScrollListener(new OnVerticalScrollListener() {

            @Override
            public void onScrolledToEnd() {
                if(isFirstLoading)
                {
                    isFirstLoading=false;
                    return;
                }
                recylerViewScrollListener();
            }

            @Override
            public void onScrolledUp() {
                super.onScrolledUp();
                recylerViewScrollListener();
            }
        });*/

    }

    abstract protected void onSwipeLayoutSetToDefaultPage();

    abstract  protected void recylerViewScrollListener();

}