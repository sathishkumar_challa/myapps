package com.sparsh.miapps.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Filter;
import android.widget.TextView;

import com.sparsh.miapps.R;
import com.sparsh.miapps.activities.MyAppsActivity;
import com.sparsh.miapps.adapters.AppListAdapter;
import com.sparsh.miapps.adapters.SectionedGridRecyclerViewAdapter;
import com.sparsh.miapps.application.MyApplication;
import com.sparsh.miapps.core.App;
import com.sparsh.miapps.database.ApplicationsDatabase;
import com.sparsh.miapps.interfaces.HomeActivityListener;
import com.sparsh.miapps.interfaces.MyTitleBarMenuListener;
import com.sparsh.miapps.synctasks.AppLoadTask;
import com.sparsh.miapps.utils.Constants;
import com.sparsh.miapps.utils.SharedPreferenceUtils;
import com.sparsh.miapps.views.IndexFastScrollRecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.PopupMenu;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

//import com.google.android.gms.ads.AdView;


public class HomeFragment extends AbstractFragment {

    private AppListAdapter adapter;
    private InputMethodManager keyBoardHide;
    //private EditText editText;
    protected IndexFastScrollRecyclerView recyclerView;
    //private AdView mAdView;
    private View fragmentView;
    private RecyclerViewObjListener objListener;
    private TextView no_apps_tv;
    private HomeFragmentListener listener;
    private GridLayoutManager manager;

    //private Button size_bt;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.my_apps_layout, container, false);

       // setupSwipeRefreshLayout(fragmentView);
        init();
        return fragmentView;
    }

   /* @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.my_apps_layout);
        init();
    }*/


    private void init() {
        // mAdView = fragmentView.findViewById(R.id.adView);
        //setupSwipeRefreshLayout(fragmentView);



        no_apps_tv = fragmentView.findViewById(R.id.no_apps_tv);
        // size_bt  =fragmentView.findViewById(R.id.size_bt);


        recyclerView = (IndexFastScrollRecyclerView) fragmentView.findViewById(R.id.my_apps_recyclerView);
        manager = new GridLayoutManager(MyApplication.getCurrentActivityContext(), SharedPreferenceUtils.getIntValueToSharedPrefarence(Constants.Pref.SPANCOUNT, 4));/*, RecyclerView.VERTICAL, false);*/

        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == 0 ? 1 : manager.getSpanCount();
            }
        });
        recyclerView.setLayoutManager(manager);

       // addListenersToRecyclerView();
       /* size_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((GridLayoutManager)recyclerView.getLayoutManager()).setSpanCount(new Random().nextInt(10));
            }
        });*/


        boolean isShowTitleBar=SharedPreferenceUtils.getBooleanValueFromSharedPrefarence(Constants.Pref.IS_SHOW_TITLE_BAR_ENABLE, true);

        if(isShowTitleBar)
        {
            recyclerView.setIndexbarMargin(recyclerView.getResources().getDimensionPixelSize(R.dimen._65dp)/*getActionbarSize()*/);


        }else {
            recyclerView.setIndexbarMargin(recyclerView.getResources().getDimensionPixelSize(R.dimen._30dp)/*getActionbarSize()*/);
        }
        recyclerView.setIndexTextSize(13);
        //recyclerView.setIndexBarCornerRadius(10);

        // if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        //recyclerView.setIndexbarHighLateTextColor(R.color.colorAccent);
        // recyclerView.setIndexTextSize(14);
        recyclerView.setIndexBarHighLateTextVisibility(true);
        recyclerView.setIndexbarHighLateTextColor("blue");

        recyclerView.setIndexBarMyTextColor(SharedPreferenceUtils.getIntValueToSharedPrefarence(getResources().getString(R.string.app_name_color), getColorCode(R.color.black)));

        // appNametextView.setTextColor(SharedPreferenceUtils.getIntValueToSharedPrefarence(itemView.getResources().getString(R.string.app_name_color), getColorCode(R.color.black,itemView)));

        /*}else
        {
            recyclerView.setIndexbarHighLateTextColor(getResources().getColor(R.color.colorAccent));

        }*/

        if(!isShowTitleBar)
        {
            try {
                recyclerView.setPadding(0, recyclerView.getResources().getDimensionPixelSize(R.dimen._48dp)/*getActionbarSize()*/, (int) convertDPToPixels(18), 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
       /* editText= (EditText) fragmentView.findViewById(R.id.search_edit_text);
        hideKeyBord(editText);


        editText.addTextChangedListener(new

                                                TextWatcher() {
                                                    @Override
                                                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                                                    }

                                                    @Override
                                                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                        //myadapter.getFilter().filter(s.toString());
                                                    }

                                                    @Override
                                                    public void afterTextChanged(Editable s) {
                                                        if(adapter!=null) {
                                                            adapter.getFilter().filter(s.toString());
                                                        }
                                                    }
                                                }

        );*/

        //@RequiresApi(api = Build.VERSION_CODES.M)
        recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyBord(recyclerView);
                if (listener != null) {
                    listener.onRecylerViewClicked(v);
                }
                return false;
            }
        });
               /* if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M )
                {
                    recyclerView.setOnScrollChangeListener(new View.OnScrollChangeListener() {

                        @Override
                        public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                            //hideKeyBord(recyclerView);
                        }
                    });
                }*/

        computeShowTitleBarView();

        IntentFilter filter = new IntentFilter(
                Constants.MY_INTENT_FILTER);
        //filter.addAction("android.intent.action.PACKAGE_REMOVED");
        // filter.addAction(Intent.ACTION_PACKAGE_ADDED);
        // filter.addAction(Intent.ACTION_PACKAGE_INSTALL);
        // filter.addDataScheme("package");
        LocalBroadcastManager.getInstance(MyApplication.getCurrentActivityContext()).registerReceiver(
                loadListReceiver, filter);
        new AppLoadTask((Activity) MyApplication.getCurrentActivityContext(), false).execute();

        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setupSwipeRefreshLayout(fragmentView);
            }
        },2000);
*/
    }

    public void setIndexBarMyTextColor() {
        if (recyclerView != null) {
            recyclerView.setIndexBarMyTextColor(SharedPreferenceUtils.getIntValueToSharedPrefarence(getResources().getString(R.string.app_name_color), getColorCode(R.color.black)));
        }

        boolean isEnable = SharedPreferenceUtils.getBooleanValueFromSharedPrefarence(Constants.Pref.IS_FREQ_USED_APPS_ENABLE, true);

    }

    private int getColorCode(int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return getResources().getColor(id, null);
        } else {
            return getResources().getColor(id);
        }
    }



    public void computeShowTitleBarView(){
        computeShowTitleBarView(false);
    }

    public void computeShowTitleBarView(boolean isShowSearchView) {
        if(recyclerView==null)
        {
            return;
        }
        boolean isShowTitleBar=SharedPreferenceUtils.getBooleanValueFromSharedPrefarence(Constants.Pref.IS_SHOW_TITLE_BAR_ENABLE, true);

        recyclerView.setIndexbarMargin(recyclerView.getResources().getDimensionPixelSize(R.dimen._40dp)/*getActionbarSize()*/);

        if(!isShowTitleBar)
        {
            //recyclerView.setIndexbarMargin(recyclerView.getResources().getDimensionPixelSize(R.dimen._65dp)/*getActionbarSize()*/);

            if(isShowSearchView)
            {
                try {
                    recyclerView.setPadding(0, recyclerView.getResources().getDimensionPixelSize(R.dimen._48dp)/*getActionbarSize()*/, (int) convertDPToPixels(18), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else
            {
                try {
                    recyclerView.setPadding(0, 0/*getActionbarSize()*/, (int) convertDPToPixels(18), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }


        }else {
            //recyclerView.setIndexbarMargin(recyclerView.getResources().getDimensionPixelSize(R.dimen._30dp)/*getActionbarSize()*/);

            try {
                recyclerView.setPadding(0, recyclerView.getResources().getDimensionPixelSize(R.dimen._48dp)/*getActionbarSize()*/, (int) convertDPToPixels(18), 0);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        recyclerView.scrollToPosition(0);
        recyclerView.invalidate();


    }


    private static final int sColumnWidth = 45; // assume cell width of 120dp

    public int getMaxSpanSize() {
        int spanCount = 0;
        try {
            spanCount = (int) Math.floor(recyclerView.getWidth() / convertDPToPixels(sColumnWidth));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return spanCount;
        // ((GridLayoutManager) recyclerView.getLayoutManager()).setSpanCount(spanCount);
    }

    private float convertDPToPixels(int dp) throws Exception {
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float logicalDensity = metrics.density;
        return dp * logicalDensity;
    }


/*    private int getActionbarSize() {
        TypedValue tv = new TypedValue();

        if (getActivity().getTheme().resolveAttribute(R.attr.actionBarSize, tv, true)) {
            return TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        }
        return 10;
    }*/


    public void onShareClick(View view) {

    }

    public void onRefreshClick(View view) {
        /*if(editText!=null)
        {
            editText.setText(null);
        }*/

        showRightMenuList(view);

        // showDialogForRefresh();

    }


    private void showDialogForRefresh(String message, final boolean isRefresh) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder((MyAppsActivity) MyApplication.getCurrentActivityContext());

        // Setting Dialog Title
        alertDialog.setTitle("Conformation");

        // Setting Dialog Message
        alertDialog.setMessage(message/**/);

        // setting Dialog cancelable to false 9010864578
        //alertDialog.setCancelable(false);

        // On pressing Settings button
        alertDialog.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        refreshAppsWithView(isRefresh);

                    }
                });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
       /* alertDialog.setNeutralButton("Share", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                shareApplication(app);
            }
        });*/

        alertDialog.show();
    }

    public void refreshAppsWithView(boolean isRefresh) {
        if (objListener != null) {
            //objListener.getRecyclerView(recyclerView);
            objListener.refreshEditext();
        }


        final ArrayList<App> recentApps = new ApplicationsDatabase(MyApplication.getCurrentActivityContext()).getRecentApplications();
                      /*  new ApplicationsDatabase(MyApplication.getCurrentActivityContext())
                                .increaseAppCount(app);*/

        AppLoadTask loadTask = new AppLoadTask((Activity) MyApplication.getCurrentActivityContext(), true);

        if (isRefresh) {
            loadTask.setAppsLoadListener(new AppLoadTask.AppsLoadListener() {
                @Override
                public void loadComplete() {
                    if (recentApps == null || recentApps.size() == 0) {
                        return;
                    }
                    final ApplicationsDatabase db = new ApplicationsDatabase(MyApplication.getCurrentActivityContext());


                    for (App app : recentApps) {
                        db.updateAppCount(app);
                    }
                }
            });
        }

        loadTask.execute();
    }

    /*@Override
    protected void onSwipeLayoutSetToDefaultPage() {
        if (swipeLayout != null) {
            swipeLayout.setRefreshing(false);
        }
        if(editText!=null)
        {
            editText.setText(null);
        }
        new AppLoadTask((Activity)MyApplication.getCurrentActivityContext(),false).execute();

    }*/

    /*@Override
    protected void recylerViewScrollListener() {

    }*/

    private BroadcastReceiver loadListReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            //addListenersToRecyclerView();

            int type = SharedPreferenceUtils.getIntValueToSharedPrefarence(Constants.AppsViewType.APPS_VIEW_TYPE_KEY, Constants.AppsViewType.A_TO_Z);
            ArrayList<App> applications1 = new ArrayList<>();
            switch (type) {
                case Constants.AppsViewType.A_TO_Z:
                    applications1 = new ApplicationsDatabase(context)
                            .getApplications();
                    sortApplications(applications1, true);

                    break;
                case Constants.AppsViewType.Z_TO_A:
                    applications1 = new ApplicationsDatabase(context)
                            .getApplications();
                    sortApplications(applications1, false);
                    break;
               /* case Constants.AppsViewType.MOST_USED:
                    ArrayList<App> applications2 = new ApplicationsDatabase(context)
                            .getAllRecentApplications();
                    ArrayList<App> applications3 = new ApplicationsDatabase(context)
                            .getAllNonRecentApplications();
                    applications1.addAll(applications2);
                    applications1.addAll(applications3);

                    break;*/

            }
           /* ArrayList<App> applications1 = new ApplicationsDatabase(context)
                    .getAllRecentApplications();*/


            boolean isEnable = SharedPreferenceUtils.getBooleanValueFromSharedPrefarence(Constants.Pref.IS_FREQ_USED_APPS_ENABLE, true);
            ArrayList<App> applications = new ArrayList<>();
            if (isEnable) {
                App app = new App(true);
                app.appName = "#";
                app.packageName = Constants.Pref.FREQ_USED_PACKAGE_NAME;
                applications.add(0, app);
            }

            applications.addAll(applications1);

            // applications.remove(applications.size()-1);

            adapter = new AppListAdapter(MyApplication.getCurrentActivityContext(),/*getMaxSpanSize(),*/ applications, new RecyclerViewObjListener() {
                @Override
                public void swipeDownRefresh() {

                    //

                }

                @Override
                public void getRecyclerView(RecyclerView recyclerView) {

                }

                @Override
                public void refreshEditext() {

                }

                @Override
                public void applicationsCount(int count) {
                    if (objListener != null) {
                        objListener.applicationsCount(count);
                    }
                    noAppsfoundView(count);
                }
            });

            //start
            List<SectionedGridRecyclerViewAdapter.Section> sections =
                    new ArrayList<SectionedGridRecyclerViewAdapter.Section>();

            //Sections
  /*          sections.add(new SectionedGridRecyclerViewAdapter.Section(0,"Section 1"));
            sections.add(new SectionedGridRecyclerViewAdapter.Section(5,"Section 2"));
            sections.add(new SectionedGridRecyclerViewAdapter.Section(12,"Section 3"));
            sections.add(new SectionedGridRecyclerViewAdapter.Section(14,"Section 4"));
*/
            sections.add(new SectionedGridRecyclerViewAdapter.Section(applications.size(), "Section 5"));

            //Add your adapter to the sectionAdapter
            SectionedGridRecyclerViewAdapter.Section[] dummy = new SectionedGridRecyclerViewAdapter.Section[sections.size()];
            SectionedGridRecyclerViewAdapter mSectionedAdapter = new
                    SectionedGridRecyclerViewAdapter(R.layout.section, recyclerView, adapter);
            mSectionedAdapter.setSections(sections.toArray(dummy));

            //end
            recyclerView.setAdapter(adapter);
            manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return adapter.isHeader(position) ? manager.getSpanCount() : 1;
                }
            });
           /* manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return position==0 ? manager.getSpanCount():1;
                }
            });*/
            //recyclerView.setAdapter(adapter);
            recyclerView.invalidate();
            // editText.setText("");          cvfddddddddddddd
            hideKeyBord(recyclerView);
            if (objListener != null) {
                objListener.getRecyclerView(recyclerView);
                objListener.refreshEditext();
                objListener.applicationsCount(applications.size());
            }
            noAppsfoundView(applications.size());


         /* final   GridLayoutManager manager=  ((GridLayoutManager) recyclerView.getLayoutManager());
            manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return adapter.isHeader(position) ? manager.getSpanCount(): 1;
                }
            });*/

            //addListenersToRecyclerView();

        }
    };

    private void sortApplications(ArrayList<App> applications1, final boolean isAToZ) {
        Collections.sort(applications1, new Comparator<App>() {

            @Override
            public int compare(App lhs, App rhs) {
                int compare = lhs.getAppName().compareToIgnoreCase(
                        rhs.getAppName());
                if (isAToZ) {
                    return compare;
                }
                if (compare == 0) {
                    return compare;
                }
                return compare * -1;
            }
        });
    }

    private void noAppsfoundView(int count) {
        if (count <= 0) {
            no_apps_tv.setVisibility(View.VISIBLE);
            if (getActivity() instanceof HomeActivityListener) {
                final HomeActivityListener listener = (HomeActivityListener) getActivity();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        listener.showToolBar();
                    }
                }, 2000);
            }

        } else {
            no_apps_tv.setVisibility(View.GONE);

        }
    }

    public void hideKeyBord(View view) {
        if (view != null) {

            if (keyBoardHide == null) {
                keyBoardHide = (InputMethodManager) MyApplication.getCurrentActivityContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                // keyBoardHide.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY,
                // 0);
            }
            if (keyBoardHide != null && keyBoardHide.isActive()) {
                // to hide keyboard
                if (view != null) {
                    keyBoardHide.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
        }
    }

    public void setOnRecyclerViewObjListener(RecyclerViewObjListener objListener) {
        this.objListener = objListener;
    }

    public void setSpanCount(int spanCount) {
        ((GridLayoutManager) recyclerView.getLayoutManager()).setSpanCount(spanCount);
        // recyclerView.getAdapter().notifyDataSetChanged();


    }

    public void setIconSize(int iconSize) {
        adapter.setIconSize(iconSize);
    }

    public void onSearchView(boolean isOpen) {
        computeShowTitleBarView(isOpen);
    }


    public interface RecyclerViewObjListener {
        void getRecyclerView(RecyclerView recyclerView);

        void refreshEditext();

        void applicationsCount(int count);

        void swipeDownRefresh();
    }

    public void searchAppByString(String str, Filter.FilterListener listener) {
        if (adapter != null) {
            adapter.getFilter().filter(str, listener);
        }
    }

    public void setOnHomeFragmentListener(HomeFragmentListener listener) {
        this.listener = listener;
    }

    public interface HomeFragmentListener {
        void onRecylerViewClicked(View view);
    }




    public MyTitleBarMenuListener getMyTitleBarMenuListener()
    {
        return new MyTitleBarMenuListener() {
            @Override
            public void actionAtoZClick() {
                SharedPreferenceUtils.setIntValueToSharedPrefarence(Constants.AppsViewType.APPS_VIEW_TYPE_KEY, Constants.AppsViewType.A_TO_Z);
                refreshAppsWithView(true);
            }

            @Override
            public void actionZtoAClick() {
                SharedPreferenceUtils.setIntValueToSharedPrefarence(Constants.AppsViewType.APPS_VIEW_TYPE_KEY, Constants.AppsViewType.Z_TO_A);
                refreshAppsWithView(true);

            }

            @Override
            public void actionRefreshClick() {
                showDialogForRefresh("Are you sure, you want to refresh the app list?", true);

            }

            @Override
            public void actionResetClick() {
                showDialogForRefresh("\'Reset\' will remove all your frequently used apps. Are you sure, you want to Reset? ", false);

            }
        };
    }



    private void showRightMenuList(View view) {
        try {

            //holder.mIndicator.setExpandedState(false,false);
            PopupMenu popup = new PopupMenu(MyApplication.getCurrentActivityContext(), view);
            popup.getMenuInflater().inflate(R.menu.search_menu, popup.getMenu());
            popup.show();
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {

                    switch (menuItem.getItemId()) {
                        case R.id.action_atoz:
                            SharedPreferenceUtils.setIntValueToSharedPrefarence(Constants.AppsViewType.APPS_VIEW_TYPE_KEY, Constants.AppsViewType.A_TO_Z);
                            refreshAppsWithView(true);
                            break;
                        case R.id.action_ztoa:
                            SharedPreferenceUtils.setIntValueToSharedPrefarence(Constants.AppsViewType.APPS_VIEW_TYPE_KEY, Constants.AppsViewType.Z_TO_A);
                            refreshAppsWithView(true);
                            break;
                        case R.id.action_refresh:
                            showDialogForRefresh("Are you sure, you want to refresh the app list?", true);

                            break;
                        case R.id.action_reset:
                            showDialogForRefresh("\'Reset\' will remove all your frequently used apps. Are you sure, you want to Reset? ", false);

                            break;


                        default:
                            break;
                    }

                    return true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        if (getView() != null) {
            ViewGroup parent = (ViewGroup) getView().getParent();
            parent.removeAllViews();
        }
        super.onDestroyView();
    }

   /* @Override
    protected void onSwipeLayoutSetToDefaultPage() {
        if(objListener!=null)
        {
            objListener.swipeDownRefresh();
        }

        if(swipeLayout!=null) {
            swipeLayout.setRefreshing(false);
        }

    }

    @Override
    protected void recylerViewScrollListener() {
        if(swipeLayout!=null) {
            swipeLayout.setRefreshing(false);
        }

    }*/

}
